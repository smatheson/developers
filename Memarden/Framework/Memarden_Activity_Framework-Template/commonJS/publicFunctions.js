﻿
// maps to functions in common.js from within iframe

var parentRef = window.parent;

    if (location.href.indexOf("multi") >= 0) {
       
        parentRef = window.parent.parent;
    } 


function map_getLessonData(){
 
    return parentRef.getLessonData();

}

function map_getLessonDataAsString() {
   
    return parentRef.getLessonDataAsString();

}

function map_getMetaData() {

    return parentRef.getMetaData();

}


function map_getMetaDataAsString() {
     
    return parentRef.getMetaDataAsString();

}


////////////////////////////////////////////////////////
//Use these functions for multi-chapter data
////////////////////////////////////////////////////////


function map_getToken() {

    return parentRef.getToken();

}

function map_getApiURL() {

    return parentRef.getApiURL();

}


function map_getNumberOfChapters() {

    return parentRef.getNumberOfChapters();

}


function map_setLessonDataByChapter(chapterID) {

    return parentRef.setLessonDataByChapter(chapterID);

}


function map_getLessonDataByChapter(chapterID) {

    return parentRef.getLessonDataByChapter(chapterID);

}


function map_getLessonDataByChapterAsString(chapterID) {
   
    return parentRef.getLessonDataByChapterAsString(chapterID);

}

function map_getNameByChapter(chapterID) {

    return parentRef.getNameByChapter(chapterID);

}

function map_getMediaByChapter(chapterID) {

    return parentRef.getMediaByChapter(chapterID);

}


function map_getMediaTypeByChapter(chapterID) {

    return parentRef.getMediaTypeByChapter(chapterID);

}

////////////////////////////////////////////////////




function map_sendReportData(verb,object, timer, questionId, userAnswerId, isCorrect,isAssessment) {

    return parentRef.sendReportData(verb,object, timer, questionId, userAnswerId,  isCorrect, isAssessment);

}


function map_startTimer(){
	
	return parentRef.startTimer();
}

function map_pauseTimer(){
	
	return parentRef.pauseTimer();
}

function map_stopTimer(){
	
	return parentRef.stopTimer();
}


function map_getGameTime(){
	
	return parentRef.getGameTime();
}

function map_timerExists(){
	
	
	return parentRef.timerExists();
}

function map_getTotalQuestions() {

    return parentRef.getTotalQuestions();

}


function map_getTotalAnswersFor(questionNumber) {

    return parentRef.getTotalAnswersFor(questionNumber);

}


function map_getCorrectAnswersFor(questionNumber) {

    return parentRef.getCorrectAnswersFor(questionNumber);

}

function map_getIncorrectAnswersFor(questionNumber) {

    return parentRef.getIncorrectAnswersFor(questionNumber);

}


function map_getActivityId(){
	
	return parentRef.getActivityId();
}


function map_getQuestionIdFor(questionNumber){
	
	return parentRef.getQuestionIdFor(questionNumber);
}

        
function map_getActivityItemIdFor(itemNumber){
	
	return parentRef.getActivityItemIdFor(itemNumber);
}

function map_getCorrectAnswerIdsFor(questionNumber){
	return parentRef.getCorrectAnswerIdsFor(questionNumber);
}

function map_getAnswerIdfor(questionNumber,answerNumber){
	return parentRef.getAnswerIdfor(questionNumber,answerNumber);
}


function map_getLanguage() {

    return parentRef.getLanguage();

}


function map_getTranslation(key) {

    return parentRef.getTranslation(key);

}



function map_getLongestQuestion() {

    return parentRef.getLongestQuestion();

}

function map_getLongestAnswer() {

    return parentRef.getLongestAnswer();

}


function map_getLongestAnswerFor(questionID) {

    return parentRef.getLongestAnswerFor(questionID);

}



function map_getMediaFile(resource, filetype, size) {

    return parentRef.getMediaFile(resource, filetype, size);

}


function map_getRuntimeDir() {

    return parentRef.getRuntimeDir();

}

function map_isInTestMode() {

    return parentRef.isInTestMode();

}

function map_getMediaURL() {

    return parentRef.getMediaURL();

}


function map_fullScreenImage(srcImg){
    parentRef.fullScreenImage(srcImg);
    return "zoomOn";

}

function map_getCommonResourcesURL() {

    return parentRef.getCommonResourcesURL();

}

function map_getDifficulty(){
	
	return parentRef.getDifficulty();
}

function map_getLessonContainerHeight() {

    return parentRef.getLessonContainerHeight();
}

function map_getLessonContainerWidth() {

    return parentRef.getLessonContainerWidth();
}