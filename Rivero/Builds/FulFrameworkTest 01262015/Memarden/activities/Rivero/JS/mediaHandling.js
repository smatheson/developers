  function playAudio(AudioURL_m4a, audioURL_ogg) {
     

        	       var mediaURL =map_getMediaURL();
        	    	var extOGG = "" ;
        	    	var extM4A = "" ;
        	    	
        	    	if(map_isInTestMode()=== true){
        	    		extOGG = ".ogg";
        	    		extM4A =".m4a"
        	    		
        	    	}
        			  //  alert("play audio: " + audioURL_ogg);
        			    var myPlayer = videojs("audio1");
        			    myPlayer.src([
        	     { type: "audio/ogg", src: mediaURL + AudioURL_m4a + extOGG },
        	      { type: "audio/mp4", src: mediaURL + audioURL_ogg + extM4A  }
        	   ]);
        			    myPlayer.play();
        			}


  function playVideo(VideoURL_m4a, VideoURL_ogg,autoplay, resize) {

      if(autoplay == undefined ){autoplay = false};
      if (resize == undefined) { resize = false };

        			    var mediaURL = map_getMediaURL();
        			    var extOGG = "";
        			    var extM4A = "";

        			    if (map_isInTestMode() === true) {
        			        extOGG = ".ogv";
        			        extM4A = ".mp4"

        			    }
        			   
        			    var myPlayer = videojs("video1");
        			    myPlayer.src([
        	     { type: "video/ogg", src: mediaURL + VideoURL_ogg + extOGG },
        	      { type: "video/mp4", src: mediaURL + VideoURL_m4a + extM4A }
        			    ]);

        			    if (resize == true) {


        			        $(".videoContent").css("width", "70%");
        			        $(".videoContent").css("height", "60%");
        			        $(".videoContent").css("left", "15%");
        			        $(".videoContent").css("top", "5%");

        			        $(".videoContent").show();

        			    }

        			    if (autoplay == true) {
        			      myPlayer.play();
        			    }
        			  
        			}

  function hideVideo() {

      if ($(".videoContent").is(":visible")) {
      var myPlayer = videojs("video1");
      myPlayer.pause();
      $(".videoContent").hide();
  }
  }


  function showVideo() {

      $(".videoContent").show();

  }