﻿

var runtimeFiles = "runtimeFiles";
var fileName = "runtimeUnity.unity3d?D=123";
var runtimeURL;
var gameRatio = 1.6;
var gameW = $(window).width();
var gameH = $(window).height();
var unityObject;


function OnUnitySend(OPC) {
    switch (OPC) {
        case "GET_LESSON":
            var JSonString = map_getLessonDataAsString();
            console.log("And the lesson data is: " + JSonString);
            SendToUnity("LESSON_DATA", JSonString);
            break;
    }
}

function SendToUnity(type, data) {
    var JSO = {
        type: type,
        data: data
    };
    var JSS = JSON.stringify(JSO);
    console.log("Sending to Unity: " + JSS);
    unityObject.getUnity().SendMessage("SceneManager", "OnJSReceive", JSS);
}

$(window).load(function () {
	// perform required sizing functions on initial load
   
});



function startUnityTemplate2() {

    if (map_getNumberOfChapters() < 0) {
        var runtimeDir = map_getRuntimeDir();
        var uHeight = parseInt(gameW / gameRatio);
        var uWidth = gameW;
        runtimeURL = runtimeFiles + "/" + runtimeDir + "/" + fileName;
        //905 560
        var config = {
            width: uWidth-15,
            height: uHeight-15,
            params: {
              enableDebugging: "1",
              logoimage: "runtimeFiles/unity_game1/Logo.png"
            }
        };
        unityObject = new UnityObject2(config);
        jQuery(function () {

            var $missingScreen = jQuery("#unityPlayer").find(".missing");
            var $brokenScreen = jQuery("#unityPlayer").find(".broken");
            $missingScreen.hide();
            $brokenScreen.hide();

            unityObject.observeProgress(function (progress) {
                switch (progress.pluginStatus) {
                    case "broken":
                        $brokenScreen.find("a").click(function (e) {
                            e.stopPropagation();
                            e.preventDefault();
                            unityObject.installPlugin();
                            return false;
                        });
                        $brokenScreen.show();
                        break;
                    case "missing":
                        $missingScreen.find("a").click(function (e) {
                            e.stopPropagation();
                            e.preventDefault();
                            unityObject.installPlugin();
                            return false;
                        });
                        $missingScreen.show();
                        break;
                    case "installed":
                        $missingScreen.remove();
                        break;
                    case "first":
                        break;
                }
            });
            unityObject.initPlugin(jQuery("#unityPlayer")[0], runtimeURL);

        });
    } else {
        alert("Multi-chapter data is not supported for this template");
    }


    window.parent.$("#lessonContainer").height(uHeight);
}



// dynamically load runtime files and start canvas
function startUnityTemplate() {

 // check for multichapter data
    if (map_getNumberOfChapters() < 0) {
        var runtimeDir = map_getRuntimeDir();
      
        var uHeight = parseInt(gameW/gameRatio);
        var uWidth = gameW;
        runtimeURL = runtimeFiles + "/" + runtimeDir + "/" + fileName;
     
        if (typeof unityObject != "undefined") {
        
            unityObject.embedUnity("unityPlayer", runtimeURL, uWidth,uHeight);

        } else {
        getUnity();
            unityObject.embedUnity("unityPlayer", runtimeURL,uWidth,uHeight);
           
        }


    } else {
        alert("Multi-chapter data is not supported for this template");
    }


    window.parent.$("#lessonContainer").height(uHeight) ;

}



function getUnity() {
    if (typeof unityObject != "undefined") {
        return unityObject.getObjectById("unityPlayer");
    }
    return null;
}

function resizeCanvasWrapper(mode) {

	
var runtimeDir = map_getRuntimeDir();
 
var lessonContainer = window.parent.$("#lessonContainer")
  
 var uWidth = lessonContainer.width();
 var uHeight = parseInt(uWidth/gameRatio);
 
 var winHeight = parent.document.body.clientHeight;
 
 if(uHeight > winHeight){
	uHeight = parseInt(winHeight *.95);
	uWidth = parseInt(uHeight * gameRatio);
	lessonContainer.width(uWidth);
 }
 

 
 runtimeURL = runtimeFiles + "/" + runtimeDir + "/" + fileName;

 if (typeof unityObject != "undefined") {
 	
     unityObject.embedUnity("unityPlayer", runtimeURL, uWidth,uHeight);

 } else {
 getUnity();
     unityObject.embedUnity("unityPlayer", runtimeURL,uWidth,uHeight);
  
 }
 

 lessonContainer.height(uHeight) ;
 if(mode=="full"){
 var marginDiff = parent.document.body.clientWidth - lessonContainer.width() 
lessonContainer.css("margin-left", marginDiff/2 + "px");
 }else{
	 lessonContainer.css("margin-left","0px");

 }
 
 
}

$(window.parent).resize(function () {

	//trigger any local resize events
    onGameWindowResize();



    
    
});

