﻿
// resize wrapper once canvas has loaded
var resizeTimer;

$(window).load(function () {
	
    resizeCanvasWrapper();
});



// dynamically load runtime files and start canvas
function startMultiTemplate(filesDir,runtimeDir,fileName,canvasID) {
        var runtimeLink = $("<script type='text/javascript' src= '" + filesDir + "/" + runtimeDir + "/" + fileName + "'>");
        $("head").append(runtimeLink);
        runCanvas(canvasID);  
        window.parent.$("screenModeButtons").css("left", 800 + "px");
}



function resizeCanvasWrapper() {
	
    var viewMode = window.parent.viewMode;
    var widthPercent = 0.64;

    if (viewMode == "full") {

        widthPercent = 0.98;
    }

   var wrapperHeight =( window.parent.$("#lessonContainer").height() * .85) - 150;   
   var wrapperWidth = window.parent.$("#lessonContainer").width();



    if (wrapperHeight > 0) {
        $("iframe").css("height", wrapperHeight + "px");
    }


    var gameW = $(window.parent).width();
    var wrapperWidth = parseInt(gameW * widthPercent);

    var marginDiff = gameW - wrapperWidth;
    var newMargin = parseInt(marginDiff / 2);

    var btnLeft = newMargin + wrapperWidth - 150;
    window.parent.$("screenModeButtons").css("left", 800 + "px");

}

$(window.parent).resize(function () {
	   onResizeCanvas();
	   resizeCanvasWrapper();
});

