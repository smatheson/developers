﻿
// resize wrapper once canvas has loaded
var resizeTimer;
var windowIsVisible = true;
var fixedRatio = 0 ;
	
$(window).load(function () {
	
	
	$("body").css("margin",0);
	$("body").css("padding",0);
    resizeCanvasWrapper();
   
});



// dynamically load runtime files and start canvas
function startStandardTemplate(filesDir,fileName) {

 
        var runtimeDir = map_getRuntimeDir();
        var runtimeLink = $("<script type='text/javascript' src= '" + filesDir + "/" + runtimeDir + "/" + fileName + "'>");
        $("head").append(runtimeLink);

// reference your own canvas start function
      //start canvas function//

}



function resizeCanvasWrapper() {
	
	console.log(window.parent.viewMode);
    var viewMode = window.parent.viewMode;
    var widthPercent = 0.64;
    
    if(viewMode == "full"){
    	
    	widthPercent = 0.98;
    }
    
	var origw = $("#canvas1").width();
    var origh = $("#canvas1").height();
    var reduceRatio ;
    if(origh > 0 && origw > 0 && fixedRatio == 0 ){
    	reduceRatio = origh / origw;
   }else{
	reduceRatio = 825 / 1100 ; 
	fixedRatio = reduceRatio;
   }
    
    var gameW = $(window.parent).width();
    var gameH = $(window.parent).height();  
    
    var wrapperWidth = parseInt(gameW * widthPercent) 
    var wrapperHeight = parseInt(wrapperWidth * reduceRatio) ;
   
   // console.log(reduceRatio + " " + wrapperHeight + " " + gameH);

if(wrapperHeight > gameH){
	
	wrapperHeight = parseInt(gameH * 0.94 )-15;
	wrapperWidth = parseInt(wrapperHeight / reduceRatio);
}
  

window.parent.$("#lessonContainer").css("height", wrapperHeight + "px");
window.parent.$("#lessonContainer").css("width", wrapperWidth + "px"); 

var marginDiff = gameW - wrapperWidth ; 
var newMargin = parseInt(marginDiff /2) ;

window.parent.$("#lessonContainer").css("left", newMargin + "px");
var btnLeft = newMargin + wrapperWidth - 150 ;
window.parent.$(".screenModeButtons").css("left", btnLeft + "px" ); 

}

$(window.parent).resize(function () {
	
	resizeCanvasWrapper();
    onResizeCanvas();

});



// Pause and resume on page becoming visible/invisible
function onVisibilityChanged() {
	
	
    if (document.hidden || document.mozHidden || document.webkitHidden || document.msHidden){
       if(windowIsVisible === true){
    	
    	map_pauseTimer();
    	
    	windowIsVisible = false;
    	}
    } else{
    	if(windowIsVisible === false){
    	
        if(map_getGameTime() > 0){
        	map_startTimer(); 
        	
        }
        windowIsVisible = true;
    	}
    }
    
    visibilityHasChanged();
};


