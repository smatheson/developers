
// event handlers
    $(document).on("metaDataLoaded", setPageValues);
    $(document).on("unloadLesson", unloadLesson);
    $(document).on("lessonMediaLoaded", onLessonMediaLoaded);
    var viewMode = "normal" ;

    function onLessonMediaLoaded(e) {

    if (lessonLoaded != true) {
        lessonLoaded = true;
      
        $("#loading").css("display", "none");
        $("#lessonFrame").attr("src", activityURL);
        $("#lessonFrame").attr("width", "100%;");
        $("#lessonFrame").attr("height", "100%");    
       
    }
}

    function unloadLesson(e){

      $("#lessonFrame").attr("src", "");
    }



    function setPageValues() {

        //set page title


            $("#lessonTitle").html(getLessonName());
            $("#lessonAuthor").html("Author: " + getLessonAuthor());
            $("#lessonActivity").html("Activity: " + getActivityName());
            $("#difficulty").html("Difficulty: " + getDifficulty());
            var lessonicon = getLessonIcon();

            if (lessonicon != '') {
                $("#lessonLogo").attr("src", lessonicon);
            } else {

                $("#lessonLogo").hide();
            }
         
    }
    
    function setScreenMode(mode){
    	
    	viewMode = mode;
		var canvas1 = $("#lessonFrame").contents().find("#canvas1");
		
		var frameWidth = $("#lessonFrame").width();
		var frameHeight = $("#lessonFrame").height();
		var canvasWidth = canvas1.width();
		var canvasHeight = canvas1.height();
		var canvasRatio = canvasHeight/canvasWidth;
		var frameRatio = frameHeight/frameWidth;
		
		var hRatio= frameHeight/canvasHeight;
		var wRatio = frameWidth/canvasWidth;
		var marginOffset;
		var newH;
		var newW;
		
    	if(mode=="full"){
    		
    		$("#btnScreenModeNormal").show();
    		$("#btnScreenModeFull").hide();
    		
    		$("#lessonContainer").css("width","99%");
    		$("#lessonContainer").css("height","96%");
    		$("#lessonContainer").css("left","0px");
    		
    		frameWidth = $("#lessonFrame").width();
    		 frameHeight = $("#lessonFrame").height();
    		 canvasWidth = canvas1.width();
    		 canvasHeight = canvas1.height();
    		 canvasRatio = canvasHeight/canvasWidth;
    		 frameRatio = frameHeight/frameWidth;
    		
    		 hRatio= frameHeight/canvasHeight;
    		 wRatio = frameWidth/canvasWidth;

    		newH = frameWidth * canvasRatio ;
    		
    		if(newH > frameHeight){
    			
    			newW = frameHeight/ canvasRatio
    		
    			
    		} 
    		
    	}
    	
if(mode=="normal"){
    		
	$("#btnScreenModeNormal").hide();
	$("#btnScreenModeFull").show();
    		
    		$("#lessonContainer").css("width","64%");
    		$("#lessonContainer").css("height","92%");
    		$("#lessonContainer").css("left","18%");
    		
    	
   		 canvas1.css("margin-left",0 + "px");
    		
    		
    	}
$("#lessonFrame")[0].contentWindow.resizeCanvasWrapper(mode);
    }

    
    
    document.addEventListener("visibilitychange", onVisibilityChanged, false);
    document.addEventListener("mozvisibilitychange", onVisibilityChanged, false);
    document.addEventListener("webkitvisibilitychange", onVisibilityChanged, false);
    document.addEventListener("msvisibilitychange", onVisibilityChanged, false);
    
    function onVisibilityChanged(){
    	
    	$("#lessonFrame")[0].contentWindow.onVisibilityChanged();
    	
    }