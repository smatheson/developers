﻿

var runtimeFiles = "runtimeFiles";
var fileName = "runtimeUnity.unity3d";
var runtimeURL;
var gameW = $(window).width();
var gameH = $(window).height();
var unityObject;
var gameRatio = 1.6;

$(window).load(function () {
	// perform required sizing functions on initial load
   
});



// dynamically load runtime files and start canvas
function startUnityTemplate() {

 // check for multichapter data
    if (map_getNumberOfChapters() < 0) {
        var runtimeDir = map_getRuntimeDir();
      
        var uHeight = parseInt(gameW/gameRatio);
        var uWidth = gameW
        runtimeURL = runtimeFiles + "/" + runtimeDir + "/" + fileName;
     
        if (typeof unityObject != "undefined") {
        
            unityObject.embedUnity("unityPlayer", runtimeURL, uWidth,uHeight);

        } else {
        getUnity();
            unityObject.embedUnity("unityPlayer", runtimeURL,uWidth,uHeight);
           
        }


    } else {
        alert("Multi-chapter data is not supported for this template");
    }

    
    window.parent.$("#lessonContainer").height(uHeight) ;
    
}



function getUnity() {
    if (typeof unityObject != "undefined") {
        return unityObject.getObjectById("unityPlayer");
    }
    return null;
}

function resizeCanvasWrapper(mode) {
		
	
var runtimeDir = map_getRuntimeDir();
 
var lessonContainer = window.parent.$("#lessonContainer")
  
 var uWidth = lessonContainer.width();
 var uHeight = parseInt(uWidth/gameRatio);
 
 var winHeight = parent.document.body.clientHeight;
 
 if(uHeight > winHeight){
	uHeight = parseInt(winHeight *.95);
	uWidth = parseInt(uHeight * gameRatio);
	lessonContainer.width(uWidth);
 }
 

 
 runtimeURL = runtimeFiles + "/" + runtimeDir + "/" + fileName;

 if (typeof unityObject != "undefined") {
 	
     unityObject.embedUnity("unityPlayer", runtimeURL, uWidth,uHeight);

 } else {
 getUnity();
     unityObject.embedUnity("unityPlayer", runtimeURL,uWidth,uHeight);
  
 }
 
 
 lessonContainer.height(uHeight) ;
 
 if(mode=="full"){
 var marginDiff = parent.document.body.clientWidth - lessonContainer.width() 
lessonContainer.css("margin-left", marginDiff/2 + "px");
 }else{
	 lessonContainer.css("margin-left","0px");
 }
 
 
}

$(window.parent).resize(function () {
	
	//trigger any local resize events
    onGameWindowResize();
   


    
    
});

