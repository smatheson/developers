﻿
// resize wrapper once canvas has loaded
var resizeTimer;

$(window).load(function () {
	
    resizeCanvasWrapper();
});



// dynamically load runtime files and start canvas
function startMultiTemplate(filesDir,runtimeDir,fileName,canvasID) {
        var runtimeLink = $("<script type='text/javascript' src= '" + filesDir + "/" + runtimeDir + "/" + fileName + "'>");
        $("head").append(runtimeLink);
        runCanvas(canvasID);  

}



function resizeCanvasWrapper() {
	
	   
   

   var wrapperHeight =( window.parent.$("#lessonContainer").height() * .85) - 150;   


    if (wrapperHeight > 0) {
        $("iframe").css("height", wrapperHeight + "px");
    }


}

$(window.parent).resize(function () {
	   onResizeCanvas();
	   resizeCanvasWrapper();
});

