﻿var developerID ;

var rootReference = getRoot();
var auditResponse;


var mainActivitiesDir = "activities";
var commonjsDir = rootReference + "/commonJS/";
var apiDir ;

var lessonData = "";
var arrLessonData = [];
var numChapters = -1;
var gameTimer = null ;
var gameTime = 0 ;
var tempTime = 0;

var lessonMetaData ="";
var activityData = "";

var token;
var activityType = "";
var langID;
var runMode;

var jsonURL;
var metaDataJsonURL;
var activityJsonURL;
var reportingJsonURL;
var localActivityJsonURL ;
var testReportURL;
var apiURL;
var mediaURL = "";
var commonResourcesURL ;
var activityURL;
var templateSkinURL;
var runtimeDir;

var mediaPercentageLoaded;
var totalMediaFiles;
var numMediaFilesLoaded;

var lessonLoaded;
var lessonDataID;
var testTemplateID;

// set url variables
var mobileURL = "https://www.google.com/intl/en/chrome/browser/mobile/";
var desktopURL = "https://www.google.ca/chrome";
var imgDir = "images/system/" ;
var defaultLessonIconURL = "lessonicons/defaultLessonIcon.jpg";
var transFileName = "translations";

// set variable from activity data since the developer id must be defined
var translationURL = "";





//initialize page when DOM has loaded
$(document).ready(function () {
//localStorage.clear();
	getStoredReports();
	
	  if (!Modernizr.webgl || !Modernizr.canvas) {

	        if (Modernizr.touch) {
	            window.location.replace("browserWarning.html?device=mobile");
	        } else {
	            window.location.replace("browserWarning.html?device=desktop");
	        }
	    
    } else {

        initializePage();

    }
});




function createBrowserAlert(downloadURL) {

    strBrowserWarning = '<div class="browserWarning" style="position:relative;display:block;z-index:2147483647 ;padding:2%;margin:0px;width:98%;background-color:#dddddd;border:solid 2px #990000;font-size:18px;" >Our site is optimized for the Google Chrome browser. The browser you are using will let you browse the site but you will not be able to play games or run activities. To get the full experience, download from: <a href="' + downloadURL + '" target="_blank"> ' + downloadURL + '</a>  </div>';

    $("body").prepend(strBrowserWarning);

}

/////////////////////////////////////
// Event Listeners
/////////////////////////////////////


$(document).on("lessonDataLoaded", onLessonDataLoaded);
$(document).on("metaDataLoaded", onMetaDataLoaded);
$(window).bind('beforeunload', unloadActivity);


function onLessonDataLoaded(e) {

    //check if both lesson data and activity data have been loaded before pre-loading media resources

    if (lessonData != '' && lessonMetaData != '') {
    	initializeTranslation();
        getMediaResources(lessonData);
        
    }
}


function onMetaDataLoaded(e) {
	
		loadActivityDetails();
	}

/////////////////////////////////////
// Initialization Functions
/////////////////////////////////////

function initializePage() {

   


// first get token and set run mode before loading data
    setToken();
    
}

//get token from url and set to test mode if no token is available
function setToken() {
    token = getParameterByName("token");
 var mode = getParameterByName("mode");
 
    if(token == '' || token === undefined){
    	if(mode=== "audit"){
    		runMode = "audit";
    	}else{
    		runMode = 'test';
    	}
        
    }else{
        runMode = 'server';
    }
   
    setJsonURL(runMode);

}

function getActivityType() {

     activityType = getParameterByName("activity");

    if (activityType !=='') {
       
    activityType = getParameterByName("activity");
    }
return activityType;

}



function setLanguage() {
		
	
    if(runMode=="test"){
    	lang = getParameterByName("langID");
    		if (lang == '' ) {
    			lang = 'en' ;
    		} else {
    			lang = getParameterByName("langID");
    			lang = lang.toLowerCase();
    		}
    }else{
    	
    	lang = lessonMetaData.lesson.locale;
        	if (lang == '' ||  lang == undefined) {
        		lang = 'en' ;
        	} else {
        		lang = lessonMetaData.lesson.locale;
        		lang = lang.toLowerCase();	
    	
        	}
    }
   langID=lang;

}


function setJsonURL(mode) {

  
    
    if(mode==="audit"){
        var devid = cleanName(getParameterByName("devid"));
        var gameid = getParameterByName("gameid");
    	runAudit(devid,gameid);
    	
    }else{
    
    if (mode == 'test') {
    	
    	testTemplateID = getParameterByName("testID");
    	
    	if(testTemplateID ==='' || testTemplateID=== undefined ){
    		testTemplateID= 1;
    	}
    	jsonURL = rootReference + "/testData/lessonData" + testTemplateID + ".txt";
        metaDataJsonURL = rootReference + "/testData/lessonMetaData" + testTemplateID + ".txt";
        mediaURL = rootReference + "/testResources/";
        activityJsonURL=  rootReference + "/testData/activityManifest.txt";
        apiURL = "/api/" ;
    } else {
       apiURL = getApiURL() ;
       jsonURL = apiURL + "?token=" + token + "&service=2" ;
       metaDataJsonURL = apiURL + "?token=" + token + "&service=1";
      
       testReportURL =  rootReference + "/testData/report1.txt";
       //use this once new api service is available 
      //  activityJsonURL = apiURL + "?token=" + token + "&service=4&activity=" + activityType ;
   // failover to use values in local activity manifest
      localActivityJsonURL=  rootReference + "/testData/activityManifest.txt";
    }

   // once token, run mode and json url are set load data



   loadMetaData();
   loadLessonData();

    }
   
}


function getApiURL(){
	
	 apiURL = getParameterByName("apiServer") + "/api/" ;
	 return apiURL;
}

// audit function to verify framework setup
function runAudit(devid,gameid){
	
	
	checkFile(rootReference + "/index.html", "Index Page");
	checkFile(rootReference + "/testData/activityManifest.txt", "Activity Data File" );
	checkFile(rootReference + "/testData/lessonData1.txt", "Default Test Data File" );
	checkFile(rootReference + "/testData/lessonMetaData1.txt", "Default Test Metadata File" );
	
	checkFile(rootReference + "/commonJS/common.js", "Main JavaScript File" );
	checkFile(rootReference + "/commonJS/publicFunctions.js", "Public Functions JavaScript File" );
	checkFile(rootReference + "/commonJS/canvasFunctions.js", "Canvas Functions JavaScript File" );
	
	
	if(devid !== '' && devid !== undefined){
		
		checkFile(rootReference + "/" + mainActivitiesDir + "/" + devid + "/template-single.html","Activity Template Page");
	//	checkFile(rootReference + "/" + mainActivitiesDir + "/" + devid + "/translations/translations.properties", "Translations File");
	
		if(gameid !== '' && gameid !== undefined){
			
			checkFile(rootReference + "/" + mainActivitiesDir + "/" + devid + "/runtimeFiles/" + gameid + "/canvasRuntime.js","Runtime File for: " + gameid);
				
		}
		
	}
	
}


function checkFile(filePath,fileAlias){
	

	$.ajax({
	    url:filePath,
	    type:'HEAD',
	    error: function()
	    {
	    	createAuditMessage(false,filePath,fileAlias);  
	    },
	    success: function()
	    {
	    	createAuditMessage(true,filePath,fileAlias); 
	    }
	});
	
	
}

function createAuditMessage(response,filePath,fileAlias){
	
	if(response === true){
		
		  $("#lessonContainer").append("<div class ='message success' >Success! The " + fileAlias + " has been installed</div>");
	}else{
		
		$("#lessonContainer").append("<div class ='message fail' >Failure! The " + fileAlias + " is not present</div>");
		
	}
	
}

// get root reference URI

function getRoot() {
    currURL = window.location.pathname;
    cutoff = currURL.indexOf('/index.html');
    rootURL = currURL.substr(0, cutoff);
    return rootURL;
}


/////////////////////////////////////
// Data Functions
/////////////////////////////////////

function loadMetaData(){

    $.ajax({
        url: metaDataJsonURL,
        dataType: 'json',
        data: {},
        success: function (data) {
            setMetaData(data);
        },
        error: function (data) { alert("Unable to access metadata."); }
    });


}


function setMetaData(data) {

    lessonMetaData = data;
    $.event.trigger({
        type: "metaDataLoaded"
    });
}


function getMetaData() {

    if (lessonMetaData != "") {
        return lessonMetaData;
    } else{ 
    return "no data is currently available";
    }
}

function getMetaDataAsString() {

    if (lessonMetaData != "") {
        strLessonMetaData = JSON.stringify(lessonMetaData);
        return strLessonMetaData;
    } else {
        return "no data is currently available";
    }

}


function loadLessonData() {
   
    $.ajax({
        url: jsonURL,
        dataType: 'json',
        data: {},
        success: function (data) {
        	setNumberOfChapters(data);
        },
        error: function (data) { alert("Unable to access lesson data."); }
    });
}






//////////
// set data for multi-chapter data set templates
//////////

function setNumberOfChapters(data) {

    if (data.chapters) {
        numChapters = data.chapters.length;
        setLessonDataArray(data,numChapters);
    } else {
          setLessonData(data);
    }

}


function getNumberOfChapters() {

    return numChapters;

}

function setLessonDataArray(data,numChapters) {

    var i;
    for (i = 0; i < numChapters; i++) {
        arrLessonData[i] = data.chapters[i];

      //  if (arrLessonData[i].resource.type == "Video") {
        //    arrLessonData[i].resource.value = mediaURL + arrLessonData[i].resource.value;

   // }

       
    }

   
    // load the first lesson data section into the lessondata public variable
    lessonData = arrLessonData[0].questions;

    $.event.trigger({
        type: "lessonDataLoaded"
    });

}

function setLessonDataByChapter(sectionID) {

   lessonData = arrLessonData[sectionID].questions;
}

function getLessonDataByChapter(sectionID) {

    return arrLessonData[sectionID].questions;
}

function getLessonDataByChapterAsString(sectionID) {

    return JSON.stringify(arrLessonData[sectionID].questions);
}

function getNameByChapter(sectionID) {

    return arrLessonData[sectionID].name;
}

function getMediaByChapter(sectionID) {

    return arrLessonData[sectionID].resource.value;
}



//////////
// set data for single data set templates 
//////////

function setLessonData(data) {

    // modify values to add alternate media directory for testing

     totalq = data.questions.length ;
     
     var i;
     for (i = 0; i < totalq; i++) {
         if (data.questions[i].question.type == "Image") {
             data.questions[i].question.value = mediaURL + data.questions[i].question.value;
         }

         ansList = data.questions[i].answers;
         numAnswers = ansList.length;
         var j;
         for (j = 0; j < numAnswers; j++) {
             if (ansList[j].type == "Image") {
                 ansList[j].value = mediaURL + ansList[j].value;
             }
         }
     }


     lessonData = data;

    $.event.trigger({
        type: "lessonDataLoaded"
    });
}


function getLessonData() {

    if (lessonData != "") {
        return lessonData;
    } else {
        return "no data is currently available";
    }

}

function getLessonDataAsString() {

    if (lessonData != "") {
        strLessonData = JSON.stringify(lessonData);
              return strLessonData;
    } else {
        return "no data is currently available";
    }

}


///////
// activity data functions
///////


function loadActivityDetails(){
		developerID = getCleanName(lessonMetaData.activity.author.name);       
		templateSkinURL = lessonMetaData.activity.htmlTemplate;	
		runtimeDir = lessonMetaData.activity.runtimeDirectory;
		
		// temporary redirection until current runtime dir info is updated
		
		if(runtimeDir.indexOf("Lessons") >=0){
			
			if(runtimeDir.indexOf("Video") >=0){
				
				runtimeDir = "" ;
				templateSkinURL = "template-multi" ;
			}else{
				runtimeDir = "flash_cards_1" ;
			}
			
		}
		
		if(runtimeDir.indexOf("Reviews") >=0){
			
			runtimeDir = "connections_1" ;
		}
		
		if(runtimeDir.indexOf("Tests") >=0){
			if(runtimeDir.indexOf("Audio") >=0){
				runtimeDir = "quiz_2" ;
			}else{
			runtimeDir = "quiz_1" ;
			}
		}
		
		


		
		activityURL = mainActivitiesDir + "/" + developerID + "/" + templateSkinURL + ".html";
		
		commonResourcesURL = rootReference + "/" + mainActivitiesDir + "/" + developerID + "/" + "commonResources/" ;

		if (runMode == "test") {
		translationURL = "activities/" + developerID + "/translations/";
		}else{
			
			translationURL = "translations/" + developerID + "/";
		}

		 $.event.trigger({
		     type: "lessonDataLoaded"
		 });
	

}


function loadlocalActivityDetails(activityID){
	
	if (runMode == "test") {
			translationURL = "activities/" + developerID + "/translations/";
			var activity = getActivityType();
			if( activity !== ''){
				runtimeDir = activity;
			}
}

}


function getCommonResourcesURL(){
	
	return  commonResourcesURL;
}

////////
// Reporting Functions
///////


function sendReportData(pverb,pobject, ptimer, objectId, userAnswerId, correctAnswerId) {
	
	if(objectId == undefined || objectId == 0){objectId = null;};
	if(userAnswerId == undefined || userAnswerId == 0){userAnswerId = null;};
	if(correctAnswerId == undefined || correctAnswerId == 0 ){correctAnswerId = null;};
	if(ptimer == undefined || ptimer == 0){ptimer = getGameTime();};
	if(pverb == undefined || pverb == 0){pverb = null;};
	if(pobject == undefined || pobject == 0){pobject = null;};
	
	//construct json string
	if(pobject == "activity" && pverb=="started"){
		
		startTimer();
	}
	
	 var userActions = {};
	 var verb = {};
	 var object = {};
	 var agent ={};
	 
	if(apiURL === undefined || apiURL === "" ){
		
		getApiURL();
	}
	 
	 agent.token = token;
	 agent.apiServer = apiURL;
	 
	verb.type = pverb;
	verb.timer = ptimer;
	verb.date = Date.now();
	
	//object.activityId = getActivityId();
	//object.itemId = questionId;
	
	
	
	
	if(pobject=== "activity"){
		object.id = getActivityId();
	}else{
		object.id = objectId;
	}
	
	
	object.type = pobject;
	object.userAnswerId = userAnswerId;
	object.correctAnswerId = correctAnswerId;
	
	userActions.agent = agent;
	userActions.verb = verb;
	userActions.object = object;
	//reportObject.userActions = userActions;
	
	// store report object in local storage
	storeReportData(JSON.stringify(userActions));
	console.log(JSON.stringify(userActions));
}

function sendActivityReports(){
	
	//response.write("send reports");
}




function sendReportCollection(dataPkg,reportToken,apiServer) {
	
	
	 reportingJsonURL =  apiServer + "?token=" + reportToken + "&service=3";
	
    $.ajax({
        url: reportingJsonURL,
        type:"POST",
        dataType: 'json',
        data: dataPkg,
        success: function (response) {
        	deleteStoredReports();
        	
        },
        error: function (response) { console.log("Unable to send report."); } 
    });
}

function storeReportData(strData){

	localStorage.setItem("memardenActivityReport-" + Date.now(), strData);
	
}

function getStoredReports(){
	var jsonPkg ={};
	var userActions = [];
	var reportToken;
	var apiServer;
	//localStorage.clear();
	for ( var i = 0, len = localStorage.length; i < len; ++i ) {
		
		if(localStorage.key(i).indexOf("memardenActivityReport") >=1000 ){
			
			// remove agent node with token but save for ajax call
			var reportItem = $.parseJSON(localStorage.getItem(localStorage.key(i))) ;
			 reportToken = reportItem.agent.token;
			apiServer = reportItem.agent.apiServer;
			 delete reportItem["agent"];
			userActions.push(reportItem);			
		}

	}
	
	jsonPkg.userActions = userActions;
	
	if(userActions.length >0 ){
		sendReportCollection(JSON.stringify(jsonPkg),reportToken,apiServer);
	}
	
}

function deleteStoredReports(){
	var itemsDeleted = 0 ;
	var len = localStorage.length;
	
	
for ( var i = 0; i < len; ++i ) {
	
	var key =localStorage.key(i - itemsDeleted) ;
	
	if(key !== null){
		localStorage.removeItem(key);
			if(key !== null && key.indexOf("memardenActivityReport") >=0 ){	
		
			localStorage.removeItem(key);
			itemsDeleted = itemsDeleted + 1;
			
			}
	}
}
//alert("deleted: " + itemsDeleted + "out of " + localStorage.length);
}



function startTimer(){
	
	  var start = new Date;
      gameTimer = setInterval(function () {
          gameTime = tempTime + Math.round((new Date - start) );
         // console.log(gameTime);
              }, 1000);
      if(getGameTime() > 0){
    	  sendReportData("resumed","activity");
      }
     
}

function pauseTimer(){
	tempTime =  gameTime;
	 clearTimeout(gameTimer);
	//gameTimer= null;
	// console.log("pause: " + gameTime + " | " + tempTime);
	 sendReportData("paused","activity");  
}

function stopTimer(){
	getGameTime();
	 tempTime =  gameTime;
	 clearTimeout(gameTimer);
	 gameTime= 0;
	 tempTime = 0;
	 gameTimer= null;
	 console.log("stop: " + gameTime + " | " + tempTime);
	
}

function getGameTime(){
	
	//console.log("game time: " + gameTime);
return gameTime;	
}

function timerExists(){
	
	if(gameTimer === null){
		return false;
	}else{
		return true;
	}

}


////////
// Q & A Functions
///////

function getTotalQuestions() {
    return lessonData.numberOfQuestions ;
}

function isQNumberValid(ordinal){
	

	if(ordinal+1 <=  getTotalQuestions() && ordinal >=0){
		return true;
	}else{
		return false;
	}
}

function getTotalAnswersFor(questionNumber) {
	if(isQNumberValid(questionNumber) == true){
    return lessonData.questions[questionNumber].numberOfAnswers;
}   else{
		
		return null;
	}
}

function getCorrectAnswersFor(questionNumber) {
	if(isQNumberValid(questionNumber) == true){
    var arrAnswers = lessonData.questions[questionNumber].answers ;
      var arrCorrectAnswers = [];
     var i;
      for (i = 0; i < arrAnswers.length;i++ ) {
          if (arrAnswers[i].isCorrectAsInteger ==1) {
              arrCorrectAnswers.push(arrAnswers[i].value);
      }
      }

      return arrCorrectAnswers;
}   else{
		
		return null;
	}

  }


function getIncorrectAnswersFor(questionNumber) {
	if(isQNumberValid(questionNumber) == true){
 var arrAnswers = lessonData.questions[questionNumber].answers ;
      var arrIncorrectAnswers = [];
   var i;
      for (i = 0; i < arrAnswers.length;i++ ) {
          if (arrAnswers[i].isCorrectAsInteger == 0) {
          arrIncorrectAnswers.push(arrAnswers[i].value);  
      }
      }

      
      return arrIncorrectAnswers;
	}   else{
		
		return null;
	}

}


function getActivityId(){
	
	return lessonData.id;
}


function getActivityItemIdFor(questionNumber){
	if(isQNumberValid(questionNumber) == true){
		
	return lessonData.questions[questionNumber].id ;
	}else{
		return null;
	}
}

function getQuestionIdFor(questionNumber){
	if(isQNumberValid(questionNumber) == true){
		
	return lessonData.questions[questionNumber].question.id ;
	}else{
		return null;
	}
}


function getCorrectAnswerIdsFor(questionNumber){
	if(isQNumberValid(questionNumber) == true){
	var arrAnswers = lessonData.questions[questionNumber].answers ;
    var arrCorrectAnswers = [];
 var i;
    for (i = 0; i < arrAnswers.length;i++ ) {
        if (arrAnswers[i].isCorrectAsInteger == 1) {
        arrCorrectAnswers.push(arrAnswers[i].id);  
    }
    }
    
    return arrIncorrectAnswers;
	}else{
		return null;
	}

}

function getAnswerIdfor(questionNumber,answerNumber){
	if(isQNumberValid(questionNumber) == true){
		if(answerNumber + 1 <= getTotalAnswersFor(questionNumber) ){
			return lessonData.questions[questionNumber].answers[answerNumber].id ;
		}else{
			return null;
		}
	
	}else{
		return null;
	}
}

/////////////////////////////////////
// Media Functions
/////////////////////////////////////

function preLoadMedia(mediaJSON) {
 totalMediaFiles = mediaJSON.files.length;  
  
    $.html5Loader({
        filesToLoad: mediaJSON,
        onBeforeLoad: function () { startMediaLoadTimer(); },
        onError: function (obj, elm) { },
        onComplete: function () {
          $.event.trigger({ type: "lessonMediaLoaded" });
        },
        onElementLoaded: function (obj, elm) {  numMediaFilesLoaded = numMediaFilesLoaded + 1;  },
        onUpdate: function (percentage) { onMediaLoadingUpdate(percentage);  }

    });

}

function startMediaLoadTimer() {

    setTimeout(checkMediaLoadSuccess, 15000);
   
}

function checkMediaLoadSuccess() {
    
  
  if( lessonLoaded != true){
    if (numMediaFilesLoaded == totalMediaFiles ) {

        $.event.trigger({ type: "lessonMediaLoaded" });

    } else {
    	
    	// for now ignore resource errors unless no resources have loaded
    	
    	if( numMediaFilesLoaded>0){
    		 $.event.trigger({ type: "lessonMediaLoaded" });
    	}else{
    		$("#loading").html("Sorry, a problem occurred while loading game resources. Please check your internet connection and try again.");    
    	}
        
    }
    }

}



function getMediaResources(data) {
var mediaList = "";
 var i;
    totalq = data.questions.length ;
    for (i = 0; i < totalq; i++) {
          if (data.questions[i].question.type == "Image" ) {
              mediaList = mediaList + '{"type":"IMAGE","source":"' +  data.questions[i].question.value + '","size":1},'; 
               }

        ansList = data.questions[i].answers;
        numAnswers = ansList.length;
        var j;
        for (j = 0; j < numAnswers; j++) {
             if (ansList[j].type == "Image" ) {
               mediaList = mediaList + '{"type":"IMAGE","source":"' +  ansList[j].value + '","size":1},' ;
    
            }
         }

    }

    mediaList = mediaList.slice(0, mediaList.length - 1);
    mediaList = '{"files":[' + mediaList + ']}';
    mediaList = JSON.parse(mediaList);
    preLoadMedia(mediaList);
  
}

function onMediaLoadingUpdate(percent) {

    $("#loading").html(percent + "% complete");

}

function getMediaFile(resource,filetype, size) {
	var mediaResource = "media.resource?type=MediaResource" ;
	var mID = getLocalParameterByName("id",resource);
	mediaResource = mediaResource + "&id=" + mID + "&mediaType=" + filetype + "&mediaSize=" + size;	
	
return 	mediaResource;
	
	
}

function getMediaURL(){
	
	return mediaURL;
}

function isInTestMode(){
	
	if(runMode==="test"){
		return true;
			}else{
				return false;
			}
}

/////////////////////////////////////
// Text Functions
/////////////////////////////////////


function initializeTranslation() {

    // initialize translations
    setLanguage();
    lang = getLanguage();
   
    jQuery.i18n.properties({
        name:  transFileName, 
        path: translationURL,
        mode: 'map',
        language: lang
    });

}


function getTranslation(key) {

    var transValue = jQuery.i18n.prop(key);
   return transValue;

}

function getLongestQuestion() {
 
    var maxQuestion = 0;
    var qLength = 0;
var i;
    for (i = 0; i < lessonData.questions.length; i++) {
    qValue = lessonData.questions[i].question.value ;
        qLength = qValue.length;
        if (qLength > maxQuestion) {
            maxQuestion = qLength;
        }
    }


    return maxQuestion;
}

function getLongestAnswerFor(questionID) {

    var maxAnswer = 0;
    var aLength = 0;
    var arrAnswers = lessonData.questions[questionID].answers ;
var i;
    for (i = 0; i < arrAnswers.length; i++) {
        aValue = arrAnswers[i].value;
        aLength = aValue.length;
        if (aLength > maxAnswer) {
            maxAnswer = aLength;
        }
    }


    return maxAnswer;

}


/////////////////////////////////////
// Unload Functions
/////////////////////////////////////



function unloadActivity(e) {

   // $.event.trigger({ type: "unloadLesson" });
	getStoredReports();
	console.log("sending reports");
	 sendReportData("exited","activity");
	return 'Close This Page?';
}


/////////////////////////////////////
// Meta Data Functions
/////////////////////////////////////

function getLessonName() {

return lessonMetaData.lesson.name ;

}


function getActivityName() {
	return lessonMetaData.activity.name ;

}

function getLessonAuthor() {

    return lessonMetaData.lesson.author.name ;
}


function getLessonIcon() {

var logoURL ;

if (lessonMetaData.lesson.logo !== '' && lessonMetaData.lesson.logo !== null) {
    logoURL = mediaURL + lessonMetaData.lesson.logo ;
}else{
logoURL = rootReference + "/" + imgDir + defaultLessonIconURL ;
}

return logoURL ;
}


function getLanguage() {

    return langID;

}

function getRuntimeDir() {

    return runtimeDir;
}

function getDifficulty() {

    return lessonMetaData.difficulty;
}

function getToken(){
return token;	
}

function cleanName(dirtyText) {
    var cleanText = dirtyText.replace(" ", "_");
    cleanText = cleanText.replace(/[|&;$%@"<>()+,]/g, "");
    return cleanText;
}