﻿
// Utility Functions




function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}




function getLocalParameterByName(name,strSource) {
    var prefix = name.length + 1 ;
    var regex;
    var results;
    
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
     regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(strSource);
     return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
   
}


function getCleanName(dirtyText){
	var cleanText = dirtyText.replace(" ","_");
	cleanText = cleanText.replace(/[|&;$%@"<>()+,]/g, "");
	return cleanText;
}

function getDisplayName(cleanText){
	var displayText = cleanText.replace("_"," ");
	return displayText;
}
