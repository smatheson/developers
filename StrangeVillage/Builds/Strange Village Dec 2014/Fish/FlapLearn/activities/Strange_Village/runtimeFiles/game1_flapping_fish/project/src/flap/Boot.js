FlapLearn = {

    // constants
    filesPath: "runtimeFiles/game1_Flapping_Fish/",
    GW: 1100,
    GH: 825,
    fTm: 1000 / 60,
    GROUND_Y: 805,
    PLR_HIT: 33,            // player hit radius

    gameSnds: [
        ["flap_1", "runtimeFiles/game1_Flapping_Fish/snds/flap_1"],
        ["flap_2", "runtimeFiles/game1_Flapping_Fish/snds/flap_2"],
        ["correct", "runtimeFiles/game1_Flapping_Fish/snds/correct"],
        ["menu_show", "runtimeFiles/game1_Flapping_Fish/snds/menu_show"],
        ["hit", "runtimeFiles/game1_Flapping_Fish/snds/hit"],
        ["grab", "runtimeFiles/game1_Flapping_Fish/snds/grab"]
    ],

    // data objects
    lessonData: null,
    metaData: null,
    mainSettingsData: null,
    questions: null,

    // global values
    diffLvl: 1,                 // difficulty
    mainFont: "",
    music: null,
    orientated: false,
    livesStart: 0,              // start lives
    gravityStr: 0.0,            // game gravity
    flapStr: 0.0,               // flap strength
    horizontalSpd: 0.0,         // horizontal move speed
    trapsMove: 0.0,             // traps movement
    obstacleDensity: 0,         // obstacles density
    groundBounce: true,         // ground bounce
    groundBounceVal: 1.0,       // ground bounce value
    dTm: 0.0,                   // delta time
    gA: false,                  // game active
    gPaused: false,             // for release "flap"
    btnSignal: null,
    gameSignal: null,
    guiOver: false,             // to disable flap on gui click
    sndPlr: null,
    mathHelper: null,
    plrX: 0,
    plrY: 0,
    gameStarted: false,

    // game values
    questionsTotal: 0,          // total received
    questionActual: 0,          // actual ID
    totalAnswers: 0,            // actual game answers - for items loop
    livesActual: 0,
    score: 0,
    plrShield: false,           // player - shield active
    worldPos: 0                 // for objects positions 
};

FlapLearn.Boot = function (game) {
};

FlapLearn.Boot.prototype = {

    preload: function () {
        this.load.image('preloaderBg', FlapLearn.filesPath+'img/preloaderBg.png');
        this.load.image('preloaderFill', FlapLearn.filesPath+'img/preloaderFill.png');
        this.load.image('bgTile', FlapLearn.filesPath+'img/bgTile.jpg');
        this.load.image('groundTile', FlapLearn.filesPath+'img/groundTile.png');
        
        // data + settings
        this.load.json('settingsData', FlapLearn.filesPath+'settings/settings.json');
        FlapLearn.lessonData = map_getLessonData();
        FlapLearn.metaData = map_getMetaData();
    },

    create: function () {
        this.input.maxPointers = 1;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;

        /*if (!this.game.device.desktop) {
            this.scale.forceOrientation(true, true);
            //this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
            //this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);            
        }*/

        this.scale.setScreenSize(true);

        // settings + questions
        this.parseGameData();

        // load fonts
        WebFont.load({
            custom: {
                families: [FlapLearn.mainFont]
            },
            active: this.fontsLoaded()
        });
    },

    fontsLoaded: function () {
        // load assets
        this.state.start('Preloader');
    },

    /*enterIncorrectOrientation: function () {
        FlapLearn.orientated = false;
        document.getElementById('orientation').style.display = 'block';
    },

    leaveIncorrectOrientation: function () {
        FlapLearn.orientated = true;
        document.getElementById('orientation').style.display = 'none';
    },*/

    /********************************************************************
    *********************************************************************
    ***************************** settings ******************************
    *********************************************************************
    *********************************************************************/

    parseGameData:function(){
        FlapLearn.mainSettingsData = this.game.cache.getJSON('settingsData');
        FlapLearn.questions = FlapLearn.lessonData.questions;
        console.log(FlapLearn.questions)
        // questions
        FlapLearn.questionsTotal = FlapLearn.questions.length;
        FlapLearn.questionActual = 1;

        // settings
        FlapLearn.diffLvl = FlapLearn.metaData.difficulty-1;
        parseDifficulty();
    }
};