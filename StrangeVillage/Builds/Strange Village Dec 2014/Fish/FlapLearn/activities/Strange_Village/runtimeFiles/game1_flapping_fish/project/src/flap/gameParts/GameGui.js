/*

TODO

- DONE

*/

FlapLearn.GameGui = function (game) {
    Phaser.Group.call(this, game);
    
    // life icos
    this.lifeIcos = [];
    for(var li = 0; li<FlapLearn.livesStart; li++){
    	var lifeIco = new FlapLearn.LifeIco(this.game, 35+40*li, 35);
    	this.add(lifeIco);
        this.lifeIcos.push(lifeIco);
    }

    // score fld
    this.scoreFld = new FlapLearn.GameTitle(this.game, this.lifeIcos[this.lifeIcos.length-1].x + 38, 15, FlapLearn.mainFont, map_getTranslation("GameLabel_Score") + ": 0", 37, "#2f6b7a");
    this.add(this.scoreFld);

    // question btn
	this.questionBtn = new Phaser.Button(this.game, FlapLearn.GW-12, 12, "mainSt", this.questionClick, this, "guiQuestionBtnDn.png", "guiQuestionBtnUp.png", "guiQuestionBtnDn.png", "guiQuestionBtnUp.png");
	this.questionBtn.events.onInputOver.add(this.btnOver, this);
    this.questionBtn.events.onInputOut.add(this.btnOut, this);
    this.questionBtn.anchor.setTo(1.0, 0.0);
	this.add(this.questionBtn);

    // pause btn
	this.pauseBtn = new Phaser.Button(this.game, FlapLearn.GW-12, 12, "mainSt", this.pauseClick, this, "guiPauseBtnDn.png", "guiPauseBtnUp.png", "guiPauseBtnDn.png", "guiPauseBtnUp.png");
    this.pauseBtn.events.onInputOver.add(this.btnOver, this);
	this.pauseBtn.events.onInputOut.add(this.btnOut, this);
    this.pauseBtn.anchor.setTo(1.0, 0.0);
	//this.add(this.pauseBtn);

    // tap msg
    this.tapMsg = new Phaser.Image(this.game, FlapLearn.GW/2, FlapLearn.GH/2, "mainSt", "tapMsg.png");
    this.tapMsg.anchor.setTo(0.5, 0.5);
    this.tapMsg.visible = false;
    this.add(this.tapMsg);

    this.tapHint = new FlapLearn.GameTitle(this.game, FlapLearn.GW/2, FlapLearn.GH/2, FlapLearn.mainFont, map_getTranslation("GameHint_Tap"), 44, "#265e6d");
    this.tapHint.setAnchor(0.5, 0.5);
    this.tapHint.visible = false;
    this.add(this.tapHint);
};

FlapLearn.GameGui.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.GameGui.prototype.constructor = FlapLearn.GameGui;

FlapLearn.GameGui.prototype.questionClick = function () {
    if(FlapLearn.gA || this.tapMsg.visible){
        FlapLearn.guiOver = false;
        FlapLearn.btnSignal.dispatch("GameQuestion");
    }    
};

FlapLearn.GameGui.prototype.pauseClick = function () {
    if(FlapLearn.gA){
        FlapLearn.guiOver = false;
        FlapLearn.btnSignal.dispatch("GamePause");
    }
};

/*------------------------ complete update ------------------------*/

FlapLearn.GameGui.prototype.completeUpdate = function () {
    this.scoreUpdate();
    this.livesUpdate();
};

/*------------------------ score update ------------------------*/

FlapLearn.GameGui.prototype.scoreUpdate = function () {
    this.scoreFld.setText(map_getTranslation("GameLabel_Score") + ": " + FlapLearn.score.toString());
};

/*------------------------ lives update ------------------------*/

FlapLearn.GameGui.prototype.livesUpdate = function () {
    for(var i=0; i < this.lifeIcos.length; i++){
        if(i < FlapLearn.livesActual){
            this.lifeIcos[i].setActive(true);
        } else {
            this.lifeIcos[i].setActive(false);
        }
    }
};

/*------------------------ btns over/out ------------------------*/

FlapLearn.GameGui.prototype.btnOver = function () {
    FlapLearn.guiOver = true;
};

FlapLearn.GameGui.prototype.btnOut = function () {
    FlapLearn.guiOver = false;
};

/*------------------------ flapMsg ------------------------*/

FlapLearn.GameGui.prototype.showFlapMsg = function (doShow) {
    this.tapMsg.visible = doShow;
    this.tapHint.visible = doShow;
};