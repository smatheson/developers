/*

TODO

- DONE

*/

FlapLearn.GroundCont = function (game) {
    Phaser.Group.call(this, game);
    
    // vars
    this.tileW = 1125;
    this.tileH = 39;

    // tiles
    this.groundTile1 = new Phaser.Image(this.game, 0, FlapLearn.GH-this.tileH, "groundTile");
    this.add(this.groundTile1);

    this.groundTile2 = new Phaser.Image(this.game, this.tileW, FlapLearn.GH-this.tileH, "groundTile");
    this.groundTile2.x = this.tileW;
    this.add(this.groundTile2);
};

FlapLearn.GroundCont.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.GroundCont.prototype.constructor = FlapLearn.GroundCont;

/*------------------------ move ------------------------*/

FlapLearn.GroundCont.prototype.moveGround = function () {
    this.groundTile1.x = -FlapLearn.worldPos % this.tileW;
    this.groundTile2.x = this.groundTile1.x + this.tileW;
};

/*------------------------ reset ------------------------*/

FlapLearn.GroundCont.prototype.resetGround = function () {
    this.groundTile1.x = 0;
    this.groundTile2.x = this.tileW;
};