/*

TODO

- DONE

*/

FlapLearn.AnswerObject = function (game, answerID, positionID) {
    Phaser.Group.call(this, game);

    // vars
    this.scaleTwn;
    this.mainTwn;

    this.maxImgW = 180;
    this.maxImgH = 120;
    this.tempScaleX = 0;
    this.tempScaleY = 0;

    this.isActive = false;
    this.upState = 0;           // update state: 0-nothing, 1-add, 2-remove
    this.ansID = answerID;
    this.posID = positionID;
    this.hideW = 120;           // for show/hide check
    this.wX = 0;
    this.xRand = -(FlapLearn.obstacleDensity * 0.1) + Math.round(Math.random() * (FlapLearn.obstacleDensity * 0.2));
    // props
    this.updateWorldX();
    this.y = 200 + Math.round(Math.random() * 460);

    // parts
    this.bgBubble = new Phaser.Image(this.game, 0, 0, "mainSt", "answerBubble.png");
    this.bgBubble.anchor.setTo(0.5, 0.5);
    this.add(this.bgBubble);

    // content
    this.ansData = FlapLearn.questions[FlapLearn.questionActual-1].answers[answerID];
    this.isCorrect = this.ansData.isCorrect;
    switch (this.ansData.type) {

        case "Text":
            this.answerFld = new FlapLearn.GameTitle(this.game, 0, 0, FlapLearn.mainFont, " " + this.ansData.value + " ", 50, "#265e6d", "center", true, 185);
            this.answerFld.setAnchor(0.46, 0.5);
            this.add(this.answerFld);
            // size limit
            this.answerFld.tryFitText(185, 125);
            break;

        case "Image":
            this.ansImg = new Phaser.Image(this.game, 0, 0, this.ansData.value);
            this.ansImg.anchor.setTo(0.5, 0.5);
            // limit img size
            this.tempScaleX = this.maxImgW/this.ansImg.width;
            this.tempScaleY = this.maxImgH/this.ansImg.height;
            if(this.tempScaleX>this.tempScaleY){
                if(this.ansImg.height > this.maxImgH){
                    this.ansImg.scale.setTo(this.tempScaleY, this.tempScaleY);
                }
            } else {
                if(this.ansImg.width > this.maxImgW){
                    this.ansImg.scale.setTo(this.tempScaleX, this.tempScaleX);
                }
            }
            this.add(this.ansImg);
            break;

        default:
            console.log("Undefined Answer Type: " + this.ansData.type);
    }
};

FlapLearn.AnswerObject.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.AnswerObject.prototype.constructor = FlapLearn.AnswerObject;

/*------------------------ update ------------------------*/

FlapLearn.AnswerObject.prototype.updateWorldX = function () {
    this.wX = (FlapLearn.GW + this.posID * FlapLearn.obstacleDensity) + this.xRand;
    this.x = this.wX - FlapLearn.worldPos;
};

FlapLearn.AnswerObject.prototype.oUpdate = function () {
	this.upState = 0;
    this.x = this.wX - FlapLearn.worldPos;
    // pos check
    if(this.x < -this.hideW){
        // loop
        this.posID += FlapLearn.totalAnswers * 2;
        this.updateWorldX();
        // remove from stage
        this.upState = 2;
        this.isActive = false;
    } else if(!this.isActive && this.x < FlapLearn.GW + this.hideW){
        // add to stage
        this.upState = 1;
        this.isActive = true;
    }

    // try hit
    if(this.isActive){
        if(FlapLearn.mathHelper.testHitRectPlr(this.x - 100, this.y -70, 200, 140)) {
            this.upState = 3;
            this.hitAnim();
        }
    }

    return this.upState;
};

/*------------------------ hit ------------------------*/

FlapLearn.AnswerObject.prototype.hitAnim = function () {
    if(this.isCorrect){
        this.mainTwn = this.game.add.tween(this);    
        this.mainTwn.to({alpha:0.0}, 290, Phaser.Easing.Sinusoidal.In);
        this.mainTwn.start();

        this.scaleTwn = this.game.add.tween(this.scale);    
        this.scaleTwn.to({x:1.5, y:1.5}, 300, Phaser.Easing.Sinusoidal.In);
        this.scaleTwn.onComplete.add(this.hitAnimEnd, this);
        this.scaleTwn.start();
    } else {
        this.mainTwn = this.game.add.tween(this);    
        this.mainTwn.to({alpha:0.0, angle: 160}, 290, Phaser.Easing.Sinusoidal.In);
        this.mainTwn.start();

        this.scaleTwn = this.game.add.tween(this.scale);    
        this.scaleTwn.to({x:0.2, y:0.2}, 300, Phaser.Easing.Sinusoidal.In);
        this.scaleTwn.onComplete.add(this.hitAnimEnd, this);
        this.scaleTwn.start();
    }
};

FlapLearn.AnswerObject.prototype.hitAnimEnd = function () {
    this.removeAll();
    this.destroy();
};