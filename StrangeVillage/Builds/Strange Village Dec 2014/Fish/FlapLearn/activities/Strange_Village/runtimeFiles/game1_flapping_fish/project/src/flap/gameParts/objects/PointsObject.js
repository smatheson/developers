/*

TODO

- DONE

*/

FlapLearn.PointsObject = function (game, worldX) {
    Phaser.Image.call(this, game, 0, 0, "mainSt", "pointBubble.png");
	
	// vars
    this.isActive = false;
    this.upState = 0;
    this.hideW = 50;           // for show/hide check
    this.wX = worldX;
    this.hitR = 35;
    this.rAng = Math.random() * Math.PI;
    this.rSpd = 0.04 + Math.round(Math.random()*2)/100;
    // props
    this.updateWorldX();
    this.anchor.setTo(0.5, 0.5);
    this.rotUp();
};

FlapLearn.PointsObject.prototype = Object.create(Phaser.Image.prototype);
FlapLearn.PointsObject.prototype.constructor = FlapLearn.PointsObject;

/*------------------------ update ------------------------*/

FlapLearn.PointsObject.prototype.updateWorldX = function () {
    this.x = this.wX - FlapLearn.worldPos;
};

FlapLearn.PointsObject.prototype.oUpdate = function () {
	this.upState = 0;
	this.x = this.wX - FlapLearn.worldPos;
    this.rotUp();
    // pos check
    if(this.x < -this.hideW){
        // loop
        this.wX += FlapLearn.totalAnswers * 2 * FlapLearn.obstacleDensity;
        this.updateWorldX();
        // remove from stage
        this.upState = 2;
        this.isActive = false;
    } else if(!this.isActive && this.x < FlapLearn.GW + this.hideW){
        // add to stage
        this.upState = 1;
        this.isActive = true;
    }

    // try hit
    if(this.isActive){
        if(FlapLearn.mathHelper.testHitCircPlr(this.x, this.y, this.hitR)) {
            this.upState = 3;
        }
    }

    return this.upState;
};

FlapLearn.PointsObject.prototype.rotUp = function () {
    this.angle = -6 + Math.cos(this.rAng) * 24;
    this.rAng += this.rSpd;
};

/*------------------------ destroy ------------------------*/

FlapLearn.PointsObject.prototype.destroyObstacle = function () {
	this.kill();
};