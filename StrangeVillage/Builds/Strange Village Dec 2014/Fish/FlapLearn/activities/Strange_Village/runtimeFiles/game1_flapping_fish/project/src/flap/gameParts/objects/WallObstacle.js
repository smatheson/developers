/*

TODO

- DONE

*/

FlapLearn.WallObstacle = function (game, wallType, positionID, txName) {
    Phaser.Image.call(this, game, 0, 0, "mainSt", txName);

    // vars
    this.isActive = false;
    this.upState = 0;
    this.wType = wallType;		// 1-top, 2-btm, 3-gate
    this.posID = positionID;
    this.hideW = 100;           // for show/hide check
    this.wX = 0;
    this.xRand = -(FlapLearn.obstacleDensity * 0.1) + Math.round(Math.random() * (FlapLearn.obstacleDensity * 0.2));
    // props
    this.updateWorldX();
	this.anchor.setTo(0.5, 0.0);

    switch(this.wType){
		case 1:
			this.y = 336;			
			break;
		case 2:
			this.y = 0;
			break;
		case 3:
			this.y = 466;
			this.scale.setTo(1.0, -1.0);
			break;
	}
};

FlapLearn.WallObstacle.prototype = Object.create(Phaser.Image.prototype);
FlapLearn.WallObstacle.prototype.constructor = FlapLearn.WallObstacle;

/*------------------------ update ------------------------*/

FlapLearn.WallObstacle.prototype.updateWorldX = function () {
    this.wX = (FlapLearn.GW + this.posID * FlapLearn.obstacleDensity) + this.xRand;
    this.x = this.wX - FlapLearn.worldPos;
};

FlapLearn.WallObstacle.prototype.oUpdate = function () {
	this.upState = 0;
	this.x = this.wX - FlapLearn.worldPos;
    // pos check
    if(this.x < -this.hideW){
        // loop
        this.posID += FlapLearn.totalAnswers * 2;
        this.updateWorldX();
        // remove from stage
        this.upState = 2;
        this.isActive = false;
    } else if(!this.isActive && this.x < FlapLearn.GW + this.hideW){
        // add to stage
        this.upState = 1;
        this.isActive = true;
    }

    // try hit
    if(this.isActive && !FlapLearn.plrShield){
        switch(this.wType){
            case 1:
                if(FlapLearn.mathHelper.testHitRectPlr(this.x - 28, this.y + 6, 54, 460)) {
                    this.upState = 3;
                }       
                break;
            case 2:
                if( FlapLearn.mathHelper.testHitRectPlr(this.x - 28, this.y + 0, 54, 225) ||
                    FlapLearn.mathHelper.testHitRectPlr(this.x - 28, this.y + 527, 54, 275)) {
                    this.upState = 3;
                }  
                break;
            case 3:
                if(FlapLearn.mathHelper.testHitRectPlr(this.x - 28, this.y - 466, 54, 460)) {
                    this.upState = 3;
                }  
                break;
        }


    }

    return this.upState;
};

/*------------------------ destroy ------------------------*/

FlapLearn.WallObstacle.prototype.destroyObstacle = function () {
	this.kill();
};