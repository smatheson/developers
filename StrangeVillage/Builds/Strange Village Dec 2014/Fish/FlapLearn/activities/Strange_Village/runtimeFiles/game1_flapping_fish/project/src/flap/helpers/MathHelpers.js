FlapLearn.MathHelpers = function () {
	this.dX = 0;
	this.dY = 0;
};

FlapLearn.MathHelpers.prototype.testHitCircPlr = function (cX, cY, cR) {
	this.dX = cX - FlapLearn.plrX;
	this.dY = cY - FlapLearn.plrY;
	if(Math.sqrt(this.dX*this.dX + this.dY*this.dY) <= cR + FlapLearn.PLR_HIT){
		return true;
	} else {
		return false;
	}
};

FlapLearn.MathHelpers.prototype.testHitRectPlr = function (rX, rY, rW, rH) {
	if(	FlapLearn.plrX >= rX-FlapLearn.PLR_HIT && 
		FlapLearn.plrX <= rX + rW + FlapLearn.PLR_HIT &&
		FlapLearn.plrY >= rY-FlapLearn.PLR_HIT && 
		FlapLearn.plrY <= rY + rH + FlapLearn.PLR_HIT ){
		return true;
	} else {
		return false;
	}
};