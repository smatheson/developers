/*

TODO

- DONE

*/

FlapLearn.ScreensCont = function (game) {
    Phaser.Group.call(this, game);

    this.isQuestion = false;
};

FlapLearn.ScreensCont.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.ScreensCont.prototype.constructor = FlapLearn.ScreensCont;

/*------------------------ update ------------------------*/

FlapLearn.ScreensCont.prototype.showScreen = function (scrnID) {
	// try hide actual
	this.isQuestion = false;
	this.tryHideActualScrn();
	// show new scrn
	switch (scrnID) {

		// question
		case 1:
			this.actualScrn = new FlapLearn.ScrnQuestion(this.game);
			this.isQuestion = true;       
			break;

		// pause
		case 2:
			this.actualScrn = new FlapLearn.ScrnPause(this.game);       
			break;

		// complete
		case 3:
			this.actualScrn = new FlapLearn.ScrnComplete(this.game);       
			break;

		// end
		case 4:
			this.actualScrn = new FlapLearn.ScrnEnd(this.game);       
			break;

		default:
		console.log("Undefined ScrnID: " + scrnID);
	}
};

FlapLearn.ScreensCont.prototype.tryHideActualScrn = function (scrnID) {
	// try hide actual
	if(this.actualScrn){
		this.actualScrn.hideScrn();
		this.actualScrn = null;
	}
};

FlapLearn.ScreensCont.prototype.lUp = function () {
	if(this.actualScrn && this.isQuestion){
		this.actualScrn.lUp();
	}
};