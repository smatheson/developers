/*

TODO

- DONE

*/

FlapLearn.ScrnEnd = function (game) {
    FlapLearn.ScreenBase.call(this, game);
    
    // parts
	this.scrnTitle = new FlapLearn.GameTitle(this.game, FlapLearn.GW/2, 280, FlapLearn.mainFont,  map_getTranslation("ScrnTitle_GameOver"), 100, "#265e6d");
    this.scrnTitle.setAnchor(0.5, 0.5);
    this.add(this.scrnTitle);

    this.hintFld = new FlapLearn.GameTitle(this.game, FlapLearn.GW/2, 420, FlapLearn.mainFont,  map_getTranslation("GameHint_PlayAgain"), 60, "#265e6d", "center", true, 600);
	this.hintFld.setAnchor(0.5, 0.5);
	this.add(this.hintFld);

	this.continueBtn = new Phaser.Button(this.game, FlapLearn.GW/2, 530, "mainSt", this.continueClick, this, "btnPlayDn.png", "btnPlayUp.png", "btnPlayDn.png", "btnPlayUp.png");
	this.continueBtn.anchor.setTo(0.5, 0.0);
	this.add(this.continueBtn);

	// show
	this.showScrn();
};

FlapLearn.ScrnEnd.prototype = Object.create(FlapLearn.ScreenBase.prototype);
FlapLearn.ScrnEnd.prototype.constructor = FlapLearn.ScrnEnd;

/*------------------------ btns ------------------------*/

FlapLearn.ScrnEnd.prototype.continueClick = function () {
	if(this.isActive){
		FlapLearn.btnSignal.dispatch("GameOverClose");
	}
};