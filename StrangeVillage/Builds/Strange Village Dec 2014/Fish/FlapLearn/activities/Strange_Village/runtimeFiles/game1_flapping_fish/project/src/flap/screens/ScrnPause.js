/*

TODO

- DONE

*/

FlapLearn.ScrnPause = function (game) {
    FlapLearn.ScreenBase.call(this, game);
    
    // parts
	this.scrnTitle = new FlapLearn.GameTitle(this.game, FlapLearn.GW/2, 300, FlapLearn.mainFont, map_getTranslation("ScrnTitle_Pause"), 100, "#265e6d");
    this.scrnTitle.setAnchor(0.5, 0.5);
    this.add(this.scrnTitle);

	this.continueBtn = new Phaser.Button(this.game, FlapLearn.GW/2, 430, "mainSt", this.continueClick, this, "btnPlayDn.png", "btnPlayUp.png", "btnPlayDn.png", "btnPlayUp.png");
	this.continueBtn.anchor.setTo(0.5, 0.0);
	this.add(this.continueBtn);

	// show
	this.showScrn();
};

FlapLearn.ScrnPause.prototype = Object.create(FlapLearn.ScreenBase.prototype);
FlapLearn.ScrnPause.prototype.constructor = FlapLearn.ScrnPause;

/*------------------------ btns ------------------------*/

FlapLearn.ScrnPause.prototype.continueClick = function () {
	if(this.isActive){
		FlapLearn.btnSignal.dispatch("PauseClose");
	}
};