/*

TODO

- DONE

*/

FlapLearn.ScrnQuestion = function (game) {
    FlapLearn.ScreenBase.call(this, game);
    
    // vars
    this.maxImgW = 500;
    this.maxImgH = 220;
    this.ansY = 0;
    this.maxQ = 4;

    // parts
	this.scrnTitle = new FlapLearn.GameTitle(this.game, FlapLearn.GW/2, 50, FlapLearn.mainFont, map_getTranslation("ScrnTitle_Question") + " " + FlapLearn.questionActual + "/" + FlapLearn.questionsTotal, 70, "#265e6d");
    this.scrnTitle.setAnchor(0.5, 0.0);
    this.add(this.scrnTitle);

    this.setQuestionContent();

	this.continueBtn = new Phaser.Button(this.game, FlapLearn.GW/2, 0, "mainSt", this.continueClick, this, "btnPlayDn.png", "btnPlayUp.png", "btnPlayDn.png", "btnPlayUp.png");
	this.continueBtn.anchor.setTo(0.5, 0.0);
	this.add(this.continueBtn);

	if(this.ansTotal<=this.maxQ){
		this.continueBtn.y = 650;
	} else {
		this.continueBtn.y = 680;
	}

	// diff
	if(!FlapLearn.gameStarted){
		this.diffSelect = new FlapLearn.DiffSelect(this.game);
		this.addChild(this.diffSelect);
	}

	// show
	this.showScrn();
};

FlapLearn.ScrnQuestion.prototype = Object.create(FlapLearn.ScreenBase.prototype);
FlapLearn.ScrnQuestion.prototype.constructor = FlapLearn.ScrnQuestion;

/*------------------------ content ------------------------*/

FlapLearn.ScrnQuestion.prototype.setQuestionContent = function () {
	switch (FlapLearn.questions[FlapLearn.questionActual-1].question.type) {

		case "Text":
			this.qContent = new FlapLearn.GameTitle(this.game, FlapLearn.GW/2, 180, FlapLearn.mainFont, FlapLearn.questions[FlapLearn.questionActual-1].question.value, 85, "#265e6d", "center", true, 800);
		    this.qContent.setAnchor(0.5, 0.0);
		    this.add(this.qContent);
		    this.ansY = 465;
		    this.scrollY = 580;
		    // size limit
            this.qContent.tryFitText(800, 190);
			break;

		case "Image":
		    this.qImage = new Phaser.Image(this.game, FlapLearn.GW/2, 160, FlapLearn.questions[FlapLearn.questionActual-1].question.value);
    		this.qImage.anchor.setTo(0.5, 0.0);
    		// size limit
    		if(this.qImage.width > this.maxImgW){
                this.qImage.scale.setTo(this.maxImgW/this.qImage.width, this.maxImgW/this.qImage.width);
            }
            if(this.qImage.height > this.maxImgH){
                this.qImage.scale.setTo(this.maxImgH/this.qImage.height, this.maxImgH/this.qImage.height);
            }             
    		this.add(this.qImage);
    		this.ansY = 490;
    		this.scrollY = 590;
			break;

		case "Sound":
		    this.sndBtn = new Phaser.Button(this.game, FlapLearn.GW/2, 180, "mainSt", this.soundClick, this, "sndQuestionPlayBtnDn.png", "sndQuestionPlayBtnUp.png", "sndQuestionPlayBtnDn.png", "sndQuestionPlayBtnUp.png");
			this.sndBtn.anchor.setTo(0.5, 0.0);
			this.add(this.sndBtn);
			this.ansY = 490;
			this.scrollY = 590;
			break;

		default:
            console.log("Undefined Question Type: " + FlapLearn.questions[FlapLearn.questionActual-1].question.type);
	}

	// answers
	this.ansPad = 250;
	this.ansTotal = FlapLearn.questions[FlapLearn.questionActual-1].answers.length;
	this.ansCont = new Phaser.Group(this.game, this);

	for(var an=0; an<this.ansTotal; an++){
		var answer = new FlapLearn.AnswerObject(this.game, an, an);
		answer.x = an*this.ansPad;
		answer.y = this.ansY;
		this.ansCont.add(answer);
	}

	// scroller
	if(this.ansTotal<=this.maxQ){
		// just center
		this.ansCont.x = FlapLearn.GW/2 - ((this.ansTotal-1)*this.ansPad)/2;
	} else {
		// add scrollbar
		this.qScroll = new FlapLearn.Scroller(this.game);
		this.qScroll.y = this.scrollY;
		this.addChild(this.qScroll);

		this.ansDiff = this.ansPad * (this.ansTotal-this.maxQ);
		this.initAnsPos = FlapLearn.GW/2 - ((this.maxQ-1)*this.ansPad)/2;
		this.ansCont.x = this.initAnsPos;
	}
};

/*------------------------ btns ------------------------*/

FlapLearn.ScrnQuestion.prototype.continueClick = function () {
	if(this.isActive){
		FlapLearn.btnSignal.dispatch("QuestionClose");
	}
};

FlapLearn.ScrnQuestion.prototype.soundClick = function () {
	if(this.isActive){
		FlapLearn.sndPlr.playGameSnd(FlapLearn.questions[FlapLearn.questionActual-1].question.value);
	}
};

FlapLearn.ScrnQuestion.prototype.lUp = function () {
	if(this.qScroll){
		this.ansCont.x = this.initAnsPos - this.ansDiff * this.qScroll.getPerc();
	}
};