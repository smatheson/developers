FlapLearn.DiffSelect = function (game) {
    Phaser.Group.call(this, game);
    
    this.diffArr = [];

    this.x = 20;
    this.y = FlapLearn.GH-85;

    this.diffTitle = new FlapLearn.GameTitle(this.game, 0, -45, FlapLearn.mainFont, map_getTranslation("GameLabel_Difficulty"), 30, "#265e6d");
    this.add(this.diffTitle);

    for(var i=0;i<5;i++){
    	var tempBtn = new FlapLearn.DiffButton(this.game,i,this);
    	tempBtn.x = i*75;
    	this.addChild(tempBtn);
    	this.diffArr.push(tempBtn);
    }

    this.diffUpdate();
};

FlapLearn.DiffSelect.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.DiffSelect.prototype.constructor = FlapLearn.DiffSelect;

FlapLearn.DiffSelect.prototype.diffUpdate = function () {
	for(var i=0;i<this.diffArr.length;i++){
		if(FlapLearn.diffLvl === i){
			this.diffArr[i].setFrame(1);
		} else {
			this.diffArr[i].setFrame(0);
		}
	}
};

/*------------------------- button -----------------------------*/

FlapLearn.DiffButton = function (game,btnId,btnParent) {
	Phaser.Group.call(this, game);
    
    this.buttonID = btnId;
    this.parentCont = btnParent;
	this.frameID = 0;	// 0-up, 1-dn

	this.btnBg = new Phaser.Sprite(this.game, 0, 0, "mainSt", "guiDiffBtnUp.png");
    this.add(this.btnBg);

    this.btnTitle = new FlapLearn.GameTitle(this.game, this.btnBg.width/2, this.btnBg.height/2, FlapLearn.mainFont, String(this.buttonID+1), 45, "#265e6d");
    this.btnTitle.setAnchor(0.5, 0.5);
    this.add(this.btnTitle);

    this.btnBg.inputEnabled = true;
    this.btnBg.events.onInputDown.add(this.dnEvent, this);
};

FlapLearn.DiffButton.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.DiffButton.prototype.constructor = FlapLearn.DiffButton;

FlapLearn.DiffButton.prototype.setFrame = function (newID) {
	if(this.frameID != newID){
		this.frameID = newID;
		if(this.frameID===0){
			this.btnBg.frameName = "guiDiffBtnUp.png";
		} else {
			this.btnBg.frameName = "guiDiffBtnDn.png";
		}
	}
};

FlapLearn.DiffButton.prototype.dnEvent = function () {
	if(this.frameID===0){
		FlapLearn.diffLvl = this.buttonID;
		parseDifficulty();
		this.parentCont.diffUpdate();
	}
};       