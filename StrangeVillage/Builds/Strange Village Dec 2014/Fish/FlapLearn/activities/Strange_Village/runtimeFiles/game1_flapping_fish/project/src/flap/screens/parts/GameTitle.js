FlapLearn.GameTitle = function (game, iX, iY, fontName, initText, textSize, txColor, align, doWrap, wrapWidth) {
    Phaser.Group.call(this, game);
    // define
	if(typeof(txColor)==='undefined') txColor = "#000000";
	if(typeof(align)==='undefined') align = "center";
	if(typeof(doWrap)==='undefined') doWrap = false;
	if(typeof(wrapWidth)==='undefined') wrapWidth = 0;

    // vars
	this.anchorX = 0;
	this.anchorY = 0;
	// props
	this.x = iX;
	this.y = iY;
    // fld
    this.btnTx = initText;
    
    if(this.btnTx.charAt(0) === " "){
    	this.btnTx = this.btnTx.slice(1,this.btnTx.length);
    }
    if(this.btnTx.charAt(this.btnTx.length-1) === " "){
    	this.btnTx = this.btnTx.slice(0,this.btnTx.length-1);
    }

    this.txFld = new Phaser.Text(this.game, 0, 0, this.btnTx, { font: textSize.toString() + "px " + fontName, fill: txColor, align: align, wordWrap:doWrap, wordWrapWidth:wrapWidth });
	this.add(this.txFld);
};

FlapLearn.GameTitle.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.GameTitle.prototype.constructor = FlapLearn.GameTitle;

/*------------------------ fc ------------------------*/

FlapLearn.GameTitle.prototype.setAnchor = function (aX, aY) {
	this.anchorX = aX;
	this.anchorY = aY;
	this.anchorUpdate();
};

FlapLearn.GameTitle.prototype.anchorUpdate = function () {
	this.txFld.updateTransform();
	this.txFld.x = -this.txFld.width * this.anchorX;
	this.txFld.y = -this.txFld.height * this.anchorY;
};

FlapLearn.GameTitle.prototype.setText = function (newText) {
	this.txFld.setText(newText);
	this.anchorUpdate();
};

FlapLearn.GameTitle.prototype.getWidth = function () {
	return this.txFld.width;
};

FlapLearn.GameTitle.prototype.tryFitText = function (maxW, maxH) {
	if(this.txFld.height > maxH){
		this.scale.setTo(maxH/this.txFld.height, maxH/this.txFld.height);
	} else if(this.txFld.width > maxW){
		this.scale.setTo(maxW/this.txFld.width, maxW/this.txFld.width);
	}
};