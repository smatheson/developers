﻿{"activities":[
{"id":11,"devID":"Memarden","supportsTemplates":[1,2,3,4],"activityName":"Flash Cards","templateSkinURL":"template-single.html","runtimeDir":"flash_cards_1"},
{"id":2,"devID":"Memarden","supportsTemplates":[1,2,3,4],"activityName":"Connect It","templateSkinURL":"template-single.html","runtimeDir":"connections_1"},
{"id":3,"devID":"Memarden","supportsTemplates":[1,2,3,4],"activityName":"Quiz","templateSkinURL":"template-single.html","runtimeDir":"quiz_1"},
{"id":6,"devID":"Memarden","supportsTemplates":[5],"activityName":"Short Answer Quiz","templateSkinURL":"template-single.html","runtimeDir":"quiz_2"},
{"id":7,"devID":"Memarden","supportsTemplates":[1],"activityName":"Memarden 500","templateSkinURL":"template-single.html","runtimeDir":"game_1"},
{"id":8,"devID":"Memarden","supportsTemplates":[1],"activityName":"Cloudblasters","templateSkinURL":"template-single.html","runtimeDir":"game_2"},
{"id":9,"devID":"Memarden","supportsTemplates":[6],"activityName":"Video and Multiple Choice","templateSkinURL":"template-multi.html","runtimeDir":"video_1"},
{"id":10,"devID":"Memarden","supportsTemplates":[7],"activityName":"Unity Game","templateSkinURL":"template-unity.html","runtimeDir":"unity_test"},
{"id":1,"devID":"Strange Village","supportsTemplates":[1,2,3,4],"activityName":"Flappping Fish","templateSkinURL":"template-single.html","runtimeDir":"game1_flapping_fish"},
]}