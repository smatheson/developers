﻿
// resize wrapper once canvas has loaded
var resizeTimer;
var windowIsVisible = true;
	
$(window).load(function () {
	
    resizeCanvasWrapper();
});



// dynamically load runtime files and start canvas
function startStandardTemplate(filesDir,fileName) {

 // check for multichapter data
    if (map_getNumberOfChapters() < 0) {
        var runtimeDir = map_getRuntimeDir();
        var runtimeLink = $("<script type='text/javascript' src= '" + filesDir + "/" + runtimeDir + "/" + fileName + "'>");
        $("head").append(runtimeLink);

// reference your own canvas start function
        runCanvas();
    } else {
        alert("Multi-chapter data is not supported for this template");
    }

}



function resizeCanvasWrapper() {	
	   /*
    var origw = $("canvas").width();
    var origh = $("canvas").height();
    var reduceRatio = origh / origw;

    var wrapperWidth = window.parent.$("#lessonContainer").width();
    var wrapperHeight = (wrapperWidth * reduceRatio);
    var winHeight = parent.document.body.clientHeight;


    if(wrapperHeight > (winHeight)){
    	wrapperHeight = winHeight * 0.95;
    	
    }
      

    //window.parent.$("#lessonContainer").css("height", wrapperHeight + "px");
      
    //window.parent.$("#lessonContainer").css("height", "950px");

    var marginDiff =  window.parent.$("#lessonContainer").width() - (wrapperHeight / reduceRatio) ;
    $("canvas").css("margin-left", marginDiff/2 + "px");
    */
}

$(window.parent).resize(function () {
	
	/*resizeCanvasWrapper();
    onResizeCanvas();*/

});



// Pause and resume on page becoming visible/invisible
function onVisibilityChanged() {
	/*
	
    if (document.hidden || document.mozHidden || document.webkitHidden || document.msHidden){
       if(windowIsVisible === true){
    	
    	map_pauseTimer();
    	
    	windowIsVisible = false;
    	}
    } else{
    	if(windowIsVisible === false){
    	
        if(map_getGameTime() > 0){
        	map_startTimer(); 
        	
        }
        windowIsVisible = true;
    	}
    }
    
    visibilityHasChanged();*/
};


