﻿
// maps to functions in common.js from within iframe



function map_getLessonData(){
 
    return getLessonData();

}

function map_getLessonDataAsString() {
   
    return getLessonDataAsString();

}

function map_getMetaData() {

    return getMetaData();

}


function map_getMetaDataAsString() {
     
    return getMetaDataAsString();

}


////////////////////////////////////////////////////////
//Use these functions for multi-chapter data
////////////////////////////////////////////////////////


function map_getToken() {

    return getToken();

}

function map_getNumberOfChapters() {

    return getNumberOfChapters();

}


function map_setLessonDataByChapter(chapterID) {

    return setLessonDataByChapter(chapterID);

}


function map_getLessonDataByChapter(chapterID) {

    return getLessonDataByChapter(chapterID);

}


function map_getLessonDataByChapterAsString(chapterID) {
   
    return getLessonDataByChapterAsString(chapterID);

}

function map_getNameByChapter(chapterID) {

    return getNameByChapter(chapterID);

}

function map_getMediaByChapter(chapterID) {

    return getMediaByChapter(chapterID);

}

////////////////////////////////////////////////////




function map_sendReportData(verb,object, timer, questionId, userAnswerId, correctAnswerId) {

    return sendReportData(verb,object, timer, questionId, userAnswerId, correctAnswerId);

}


function map_startTimer(){
	
	return startTimer();
}

function map_pauseTimer(){
	
	return pauseTimer();
}

function map_stopTimer(){
	
	return stopTimer();
}


function map_getGameTime(){
	
	return getGameTime();
}

function map_timerExists(){
	
	
	return timerExists();
}

function map_getTotalQuestions() {

    return getTotalQuestions();

}


function map_getTotalAnswersFor(questionNumber) {

    return getTotalAnswersFor(questionNumber);

}


function map_getCorrectAnswersFor(questionNumber) {

    return getCorrectAnswersFor(questionNumber);

}

function map_getIncorrectAnswersFor(questionNumber) {

    return getIncorrectAnswersFor(questionNumber);

}


function map_getActivityId(){
	
	return getActivityId();
}


function map_getQuestionIdFor(questionNumber){
	
	return getQuestionIdFor(questionNumber);
}

        
function map_getActivityItemIdFor(itemNumber){
	
	return getActivityItemIdFor(itemNumber);
}

function map_getCorrectAnswerIdsFor(questionNumber){
	return getCorrectAnswerIdsFor(questionNumber);
}

function map_getAnswerIdfor(questionNumber,answerNumber){
	return getAnswerIdfor(questionNumber,answerNumber);
}


function map_getLanguage() {

    return getLanguage();

}


function map_getTranslation(key) {

    return getTranslation(key);

}



function map_getLongestQuestion() {

    return getLongestQuestion();

}


function map_getLongestAnswerFor(questionID) {

    return getLongestAnswerFor(questionID);

}



function map_getMediaFile(resource, filetype, size) {

    return getMediaFile(resource, filetype, size);

}


function map_getRuntimeDir() {

    return getRuntimeDir();

}

function map_isInTestMode() {

    return isInTestMode();

}

function map_getMediaURL() {

    return getMediaURL();

}

function map_getCommonResourcesURL() {

    return getCommonResourcesURL();

}

function map_getDifficulty(){
	
	return getDifficulty();
}