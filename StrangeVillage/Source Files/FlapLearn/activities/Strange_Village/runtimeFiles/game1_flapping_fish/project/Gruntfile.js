module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: [  "src/libs/webfont.js",
                "src/libs/phaser.js", 
                
                "src/flap/Boot.js",
                "src/flap/Preloader.js",
                "src/flap/Game.js",

                "src/flap/helpers/SoundPlayer.js",
                "src/flap/helpers/MathHelpers.js",

                "src/flap/gameParts/objects/AnswerObject.js",
                "src/flap/gameParts/objects/EnemyObstacle.js",
                "src/flap/gameParts/objects/LifeIco.js",
                "src/flap/gameParts/objects/WallObstacle.js",
                "src/flap/gameParts/objects/PointsObject.js",

                "src/flap/gameParts/BgCont.js",
                "src/flap/gameParts/ObstaclesCont.js",
                "src/flap/gameParts/PlayerMain.js",
                "src/flap/gameParts/GroundCont.js",
                "src/flap/gameParts/GameGui.js",

                "src/flap/screens/parts/GameTitle.js",  
                "src/flap/screens/parts/DiffSelect.js",  
                "src/flap/screens/parts/Scroller.js",  

                "src/flap/screens/ScreenBase.js", 
                "src/flap/screens/ScrnComplete.js", 
                "src/flap/screens/ScrnEnd.js",  
                "src/flap/screens/ScrnPause.js",  
                "src/flap/screens/ScrnQuestion.js", 
                "src/flap/screens/ScreensCont.js",

                "src/main.js"
        ],
      	dest: '../canvasRuntime.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.registerTask('default', ['concat']);
};