
FlapLearn.Game = function (game) {
    this.game;		//	a reference to the currently running game
    this.add;		//	used to add sprites, text, groups, etc
    this.camera;	//	a reference to the game camera
    this.cache;		//	the game cache
    this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;		//	for preloading assets
    this.math;		//	lots of useful common math operations
    this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
    this.stage;		//	the game stage
    this.time;		//	the clock
    this.tweens;	//	the tween manager
    this.world;		//	the game world
    this.particles;	//	the particle manager
    this.physics;	//	the physics manager
    this.rnd;		//	the repeatable random number generator

    this.bgCont;
    this.obstaclesCont;
    this.playerMain;
    this.groundCont;
    this.gameGui;
    this.scrnsCont;
    this.spaceKey;
};

FlapLearn.Game.prototype = {

	create: function () {
        // snd player
        FlapLearn.sndPlr = new FlapLearn.SoundPlayer(this.game);
        FlapLearn.mathHelper = new FlapLearn.MathHelpers();

        // set objects
        this.bgCont = new FlapLearn.BgCont(this.game);
        this.obstaclesCont = new FlapLearn.ObstaclesCont(this.game);

        this.playerMain = new FlapLearn.PlayerMain(this.game);
        this.game.add.existing(this.playerMain);

        this.groundCont = new FlapLearn.GroundCont(this.game);
        this.gameGui = new FlapLearn.GameGui(this.game);
        this.scrnsCont = new FlapLearn.ScreensCont(this.game);

        // controls
        this.spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.spaceKey.onDown.add(this.tryFlap, this);
        this.spaceKey.onUp.add(this.tryUnFlap, this);

        this.game.input.onDown.add(this.tryFlap, this);
        this.game.input.onUp.add(this.tryUnFlap, this);

        // callbacks
        FlapLearn.btnSignal = new Phaser.Signal();
        FlapLearn.gameSignal = new Phaser.Signal();
        FlapLearn.btnSignal.add(this.btnCallback, this);
        FlapLearn.gameSignal.add(this.gameCallback, this);

        // set lvl/question 1
        this.resetGameVars();
        this.setQuestionLevel();

        // show first question
        this.scrnsCont.showScreen(1);
	},

    /*------------------------ game reset ------------------------*/

    resetGameVars: function () {
        // vars
        FlapLearn.livesActual = FlapLearn.livesStart;
        FlapLearn.questionActual = 1;
        FlapLearn.score = 0;
        FlapLearn.gameStarted = false;
        // objects
        this.gameGui.completeUpdate();
    },

    /*------------------------ question/level ------------------------*/

    setQuestionLevel: function () {
        // vars
        FlapLearn.worldPos = 0;
        // parts
        this.obstaclesCont.setLevel();
        this.playerMain.resetPlr();
        this.bgCont.resetBg();
        this.groundCont.resetGround();
    },

    /*------------------------ flap ------------------------*/

    tryFlap: function () {
        if(!FlapLearn.guiOver){
            if (FlapLearn.gA) {
                this.playerMain.flap();
            } else if(FlapLearn.gPaused){
                // unpause
                FlapLearn.gA = true;
                FlapLearn.gPaused = false;
                this.gameGui.showFlapMsg(false);
                // started check
                if(!FlapLearn.gameStarted){
                    FlapLearn.gameStarted = true;
                }
                // init flap
                this.tryFlap();
            }
        }
    },

    tryUnFlap: function () {
        this.playerMain.unFlap();
    },

    /*------------------------ pause ------------------------*/

    setPauseState: function () {
        FlapLearn.gPaused = true;
        this.gameGui.showFlapMsg(true);
    },

    /********************************************************************
    *********************************************************************
    ***************************** btnCalls ******************************
    *********************************************************************
    *********************************************************************/

    btnCallback: function (callName) {
        switch (callName) {

            /*--- question ---*/

            case "QuestionClose":
                // reset level after start close (for possible difficulty change)
                if(!FlapLearn.gameStarted){
                    this.resetGameVars();
                    this.setQuestionLevel();
                }
                // hide scrn
                this.scrnsCont.tryHideActualScrn();
                // enable game
                this.setPauseState();
                break;

            /*--- pause ---*/

            case "PauseClose":
                // hide scrn
                this.scrnsCont.tryHideActualScrn();
                // enable game
                this.setPauseState();
                break;  

            /*--- game over ---*/

            case "GameOverClose":
                // reset game
                this.resetGameVars();
                this.setQuestionLevel();
                // show question
                this.scrnsCont.showScreen(1);
                break;  

            /*--- game complete ---*/

            case "GameCompleteClose":
                // reset game
                this.resetGameVars();
                this.setQuestionLevel();
                // show question
                this.scrnsCont.showScreen(1);
                break; 

            /*--- game ---*/

            case "GameQuestion":
                // pause game
                FlapLearn.gA = false;
                FlapLearn.gPaused = false;
                // show question
                this.scrnsCont.showScreen(1);
                break;

            case "GamePause":
                // pause game
                FlapLearn.gA = false;
                // show pause menu
                this.scrnsCont.showScreen(2);
                break;

            default:
            console.log("Undefined BtnCall: " + callName);
        }
    },

    /********************************************************************
    *********************************************************************
    **************************** gameCalls ******************************
    *********************************************************************
    *********************************************************************/
    
    gameCallback: function (callName) {
        switch (callName) {

            /*--- score ---*/

            case "ScoreUpdate":
                this.gameGui.scoreUpdate();
                break;

            /*--- obstacle hit ---*/

            case "ObstacleHit":
                // freeze game
                FlapLearn.gA = false;
                // anim
                this.playerMain.playHitAnim();
                break;

            case "ObstacleHitAnimEnd":
                // lives update
                FlapLearn.livesActual -= 1;
                this.gameGui.livesUpdate();
                // check end
                if(FlapLearn.livesActual == 0){
                    // game over
                    this.scrnsCont.showScreen(4);
                } else {
                    // continue -> add shield
                    this.playerMain.resetPlr();
                    this.playerMain.addShield();
                    // pause
                    this.setPauseState();
                }
                break;

            /*--- answers hit ---*/

            case "AnswerHitCorrect":
                // freeze game
                FlapLearn.gA = false;
                // anim
                this.playerMain.playAnswerCorrectHitAnim();
                break;

             case "AnswerHitAnimEnd":
                // try next question
                if(FlapLearn.questionActual == FlapLearn.questionsTotal){
                    // complete
                    this.scrnsCont.showScreen(3);
                } else {
                    // next
                    FlapLearn.questionActual += 1;
                    this.setQuestionLevel();
                    // show question
                    this.scrnsCont.showScreen(1);
                }
                break;             

            default:
            console.log("Undefined GameCall: " + callName);
        }
    },

    /********************************************************************
    *********************************************************************
    ******************************* loop ********************************
    *********************************************************************
    *********************************************************************/

	update: function () {
        FlapLearn.dTm = this.game.time.elapsed / FlapLearn.fTm;
        this.scrnsCont.lUp();

        if (FlapLearn.gA) {
            // main pos update
            FlapLearn.worldPos += FlapLearn.horizontalSpd * FlapLearn.dTm;
            // parts update
            this.bgCont.moveBg();
            this.groundCont.moveGround();
            if(this.playerMain.playerUpdate()){
                // don't update on ground hit
                this.obstaclesCont.obsUpdate();
            }            
        }
	}
};