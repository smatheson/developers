/*

TODO

- check sounds loading compatibility (disable game without webAudio?) - show warning?

*/

FlapLearn.Preloader = function (game) {

	this.background1 = null;
	this.background2 = null;
	this.preloadBg = null;
	this.preloadFill = null;

};

FlapLearn.Preloader.prototype = {

	preload: function () {

		this.background1 = this.add.sprite(0, 0, "bgTile");
		this.background2 = this.add.sprite(0, FlapLearn.GH-39, "groundTile");
		this.preloadBg = this.add.sprite(FlapLearn.GW/2-240/2, FlapLearn.GH/2, "preloaderBg");
		this.preloadBg.anchor.setTo(0.0, 0.5);
		this.preloadFill = this.add.sprite(FlapLearn.GW/2-230/2, FlapLearn.GH/2, "preloaderFill");
		this.preloadFill.anchor.setTo(0.0, 0.5);
		this.load.setPreloadSprite(this.preloadFill);

		this.load.image('scrnOverlay', FlapLearn.filesPath+'img/scrnOverlay.png');

		this.load.atlasXML("mainSt", FlapLearn.filesPath+"img/mainSt.png", FlapLearn.filesPath+"img/mainSt.xml", null);

		// preload + set question content
		var tempName = "";
		for(var qt = 0; qt < FlapLearn.questions.length; qt ++){
			// question
			if(FlapLearn.questions[qt].question.type == "Image"){
				// image
				tempName = "qi" + qt.toString();
				this.load.image(tempName, FlapLearn.questions[qt].question.value);
				FlapLearn.questions[qt].question.value = tempName;
			} else if(FlapLearn.questions[qt].question.type == "Sound"){
				// sound
				tempName = "qs" + qt.toString();
				FlapLearn.gameSnds.push([tempName, "/FlapLearn/testResources/" + FlapLearn.questions[qt].question.value]);
				FlapLearn.questions[qt].question.value = tempName;
			}
			// answers
			for(var qs = 0; qs < FlapLearn.questions[qt].answers.length; qs++){
				if(FlapLearn.questions[qt].answers[qs].type == "Image"){
					tempName = "q" + qt.toString() + "a" + qs.toString();
					this.load.image(tempName, FlapLearn.questions[qt].answers[qs].value);
					FlapLearn.questions[qt].answers[qs].value = tempName;
				}
			}
		}

		// load sounds
		if (this.game.device.webAudio) {
			for (var sn = 0; sn < FlapLearn.gameSnds.length; sn++) {
				this.load.audio(FlapLearn.gameSnds[sn][0], [FlapLearn.gameSnds[sn][1] + ".m4a", FlapLearn.gameSnds[sn][1] + ".ogg"]);
			}
		}
	},

	create: function () {
		this.background1.kill();
		this.background2.kill();
		this.preloadBg.kill();
		this.preloadFill.kill();

		this.state.start('Game');
	}
};
