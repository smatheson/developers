/*

TODO

- DONE

*/

FlapLearn.BgCont = function (game) {
    Phaser.Group.call(this, game);
     
    // vars
    this.tileW = 1125;

    // tiles
    this.bgTile1 = new Phaser.Image(this.game, 0, 0, "bgTile");
    this.add(this.bgTile1);

    this.bgTile2 = new Phaser.Image(this.game, 0, 0, "bgTile");
    this.bgTile2.x = this.tileW;
    this.add(this.bgTile2);
};

FlapLearn.BgCont.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.BgCont.prototype.constructor = FlapLearn.BgCont;

/*------------------------ move ------------------------*/

FlapLearn.BgCont.prototype.moveBg = function () {
    this.bgTile1.x = -(FlapLearn.worldPos * 0.5) % this.tileW;
    this.bgTile2.x = this.bgTile1.x + this.tileW;
};

/*------------------------ reset ------------------------*/

FlapLearn.BgCont.prototype.resetBg = function () {
    this.bgTile1.x = 0;
    this.bgTile2.x = this.tileW;
};