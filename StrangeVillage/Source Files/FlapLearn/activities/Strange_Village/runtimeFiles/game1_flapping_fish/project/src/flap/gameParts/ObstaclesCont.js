/*

TODO

- answers destroy problem (on remove old)

*/

FlapLearn.ObstaclesCont = function (game) {
    Phaser.Group.call(this, game);
    // vars
    this.answersArr = [];
    this.obstaclesArr = [];
    this.pointsArr = [];
    this.answersIDs = [];		// for random init order
    this.obsIDs = [];			// for obstacle type randomise
    this.lastObsID = -1;		// to prevent duplicity in loop
    this.wallIDs = [];			// for wall types loop
    this.lastWallID = -1;		// wall types loop
    this.tempObsID = 0;			
    this.tempObsPosID = 0;	
    this.upRetTemp = 0;			// temp var for items add/remove		
};

FlapLearn.ObstaclesCont.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.ObstaclesCont.prototype.constructor = FlapLearn.ObstaclesCont;

/*------------------------ set ------------------------*/

FlapLearn.ObstaclesCont.prototype.setLevel = function () {
	// vars
	FlapLearn.totalAnswers = FlapLearn.questions[FlapLearn.questionActual-1].answers.length;
	// parts
	this.setPointBubbles();
	this.setObstacles();
	this.setAnswers();
};

/********************************************************************
*********************************************************************
***************************** answers *******************************
*********************************************************************
*********************************************************************/

FlapLearn.ObstaclesCont.prototype.setAnswers = function () {
	// try remove old
	while(this.answersArr.length > 0){
		var tempAns = this.answersArr.pop();
		tempAns.removeAll();
		if(tempAns.isActive){
			this.remove(tempAns);
		}
	}

	// set
	for(var an=0; an<FlapLearn.totalAnswers; an++){
		this.answersIDs.push(an);
	}
	while(this.answersIDs.length > 0){
		var answer = new FlapLearn.AnswerObject(this.game, this.answersIDs.splice(Math.round(Math.random()*(this.answersIDs.length-1)),1)[0], 2 + this.answersArr.length * 2);
    	this.answersArr.push(answer);
    	// bubble
    	if(answer.y > 430){
			this.addPointBubble(answer.wX, 130 + Math.round(Math.random() * (answer.y - 160 - 130)));
    	} else {
    		this.addPointBubble(answer.wX, 700 - Math.round(Math.random() * (700 - answer.y - 160)));
    	}
	}
};

FlapLearn.ObstaclesCont.prototype.moveAnswers = function () {
	for(var i = this.answersArr.length-1; i>=0; i--){
		this.upRetTemp = this.answersArr[i].oUpdate();
		if(this.upRetTemp == 1){
			this.add(this.answersArr[i]);
		} else if(this.upRetTemp == 2){
			this.remove(this.answersArr[i]);
		} else if(this.upRetTemp == 3){
			// hit
			var tempAns = this.answersArr.splice(i,1)[0];
			// check result
			if(tempAns.isCorrect){
				// correct
				FlapLearn.score += 100;
				FlapLearn.gameSignal.dispatch("AnswerHitCorrect");
				FlapLearn.sndPlr.playGameSnd("correct");
			} else {
				// incorrect (like obstacle for now)
				FlapLearn.gameSignal.dispatch("ObstacleHit");
				FlapLearn.sndPlr.playGameSnd("hit");
			}
		}
	}
};

/********************************************************************
*********************************************************************
**************************** obstacles ******************************
*********************************************************************
*********************************************************************/

FlapLearn.ObstaclesCont.prototype.setObstacles = function () {
	// try remove old
	while(this.obstaclesArr.length > 0){
		this.obstaclesArr.pop().destroyObstacle();
	}
	// set
	for(var ob=0; ob<FlapLearn.totalAnswers; ob++){
		this.tempObsID = this.getObstacleID();
		this.tempObsPosID = 1 + ob * 2;
		var obstacle;
		switch(this.tempObsID){
			case 1:
				obstacle = new FlapLearn.WallObstacle(this.game, 1, this.tempObsPosID, "obstacle_1_" + this.getWallType() + ".png");
				break;
			case 2:
				obstacle = new FlapLearn.WallObstacle(this.game, 2, this.tempObsPosID, "obstacle_2_" + this.getWallType() + ".png");
				break;
			case 3:
				obstacle = new FlapLearn.WallObstacle(this.game, 3, this.tempObsPosID, "obstacle_1_" + this.getWallType() + ".png");
				break;
			case 4:
				obstacle = new FlapLearn.EnemyObstacle(this.game, 1, this.tempObsPosID);
				break;
			case 5:
				obstacle = new FlapLearn.EnemyObstacle(this.game, 2, this.tempObsPosID);
				break;
			case 6:
				obstacle = new FlapLearn.EnemyObstacle(this.game, 3, this.tempObsPosID);
				break;
		}
    	this.obstaclesArr.push(obstacle);
    	// add bubble
    	switch(this.tempObsID){
			case 1:
				this.addPointBubble(obstacle.wX, 130 - 40 + Math.round(Math.random()*80));
				break;
			case 2:
				this.addPointBubble(obstacle.wX, 345 - 50 + Math.round(Math.random()*100));
				break;
			case 3:
				this.addPointBubble(obstacle.wX, 605 - 40 + Math.round(Math.random()*80));
				break;
			case 4:
			case 5:
			case 6:
				this.addPointBubble(obstacle.wX, 120 + Math.round(Math.random() * 500));
				break;
		}
	}
};

FlapLearn.ObstaclesCont.prototype.moveObstacles = function () {
	for(var i = this.obstaclesArr.length-1; i>=0; i--){
		this.upRetTemp = this.obstaclesArr[i].oUpdate();
		if(this.upRetTemp == 1){
			this.add(this.obstaclesArr[i]);
		} else if(this.upRetTemp == 2){
			this.remove(this.obstaclesArr[i]);
		} else if(this.upRetTemp == 3){
			// hit
			FlapLearn.gameSignal.dispatch("ObstacleHit");
			FlapLearn.sndPlr.playGameSnd("hit");
		}
	}
};

/*------------------------ IDs ------------------------*/

FlapLearn.ObstaclesCont.prototype.getObstacleID = function () {
	if(this.obsIDs.length ==0){
		for(var i=1; i<=6; i++){
			if(i!= this.lastObsID){
				this.obsIDs.push(i);
			}
		}
	}

	this.lastObsID = this.obsIDs.splice(Math.round(Math.random()*(this.obsIDs.length-1)),1)[0];
	return this.lastObsID;
};

/*------------------------ get wall type ------------------------*/

FlapLearn.ObstaclesCont.prototype.getWallType = function () {
	if(this.wallIDs.length ==0){
		for(var i=1; i<=3; i++){
			if(i!= this.lastWallID){
				this.wallIDs.push(i);
			}
		}
	}

	this.lastWallID = this.wallIDs.splice(Math.round(Math.random()*(this.wallIDs.length-1)),1)[0];
	return this.lastWallID.toString();
};

/********************************************************************
*********************************************************************
************************** point bubbles ****************************
*********************************************************************
*********************************************************************/

FlapLearn.ObstaclesCont.prototype.setPointBubbles = function () {
	// try remove old
	while(this.pointsArr.length > 0){
		this.pointsArr.pop().destroyObstacle();
	}
	// set
	for(var pt=0; pt<FlapLearn.totalAnswers*2; pt++){
		this.addPointBubble(FlapLearn.GW - FlapLearn.obstacleDensity/2 + (1 + pt) * FlapLearn.obstacleDensity, 120 + Math.round(Math.random() * 500));
	}
};

FlapLearn.ObstaclesCont.prototype.addPointBubble = function (worldX, bubbleY) {
	var pointObj = new FlapLearn.PointsObject(this.game, worldX);
	pointObj.y = bubbleY;
    this.pointsArr.push(pointObj);
};

FlapLearn.ObstaclesCont.prototype.movePointBubbles = function () {
	for(var i = this.pointsArr.length-1; i>=0; i--){
		this.upRetTemp = this.pointsArr[i].oUpdate();
		if(this.upRetTemp == 1){
			this.add(this.pointsArr[i]);
		} else if(this.upRetTemp == 2){
			this.remove(this.pointsArr[i]);
		} else if(this.upRetTemp == 3){
			// hit
			this.pointsArr.splice(i,1)[0].destroyObstacle();
			// score update
			FlapLearn.score += 10;
			FlapLearn.gameSignal.dispatch("ScoreUpdate");
			FlapLearn.sndPlr.playGameSnd("grab");
		}
	}
};

/********************************************************************
*********************************************************************
****************************** update *******************************
*********************************************************************
*********************************************************************/

FlapLearn.ObstaclesCont.prototype.obsUpdate = function () {
	this.moveAnswers();
	this.moveObstacles();
	this.movePointBubbles();
};