/*

TODO

- DONE

*/

FlapLearn.PlayerMain = function (game) {
    Phaser.Image.call(this, game, 0, 0, "mainSt", "playerFish_normal.png");

    // vars
    this.animTwn1;
    this.animTwn2;

    this.initX = 160;
    this.initY = 370;
    this.shieldInitTime = 3000;
    this.upRet = true;				// update return

    this.vY = 0;
    this.gAng = 0;					// goal angle
    this.vA = 0.0;					// angle velocity
    this.shieldActual = 0;
    this.flapDn = false;			// flap state

    // props
    this.x = this.initX;
    this.y = this.initY;
    this.anchor.setTo(69/157, 47/79);
};

FlapLearn.PlayerMain.prototype = Object.create(Phaser.Image.prototype);
FlapLearn.PlayerMain.prototype.constructor = FlapLearn.PlayerMain;

/*------------------------ reset ------------------------*/

FlapLearn.PlayerMain.prototype.resetPlr = function () {
	// position
	this.x = this.initX;
    this.y = this.initY;
    this.vY = 0;
    this.gAng = 0;
    this.vA = 0.0;
    this.angle = 0;
    this.flapDn = false;
    this.frameName = "playerFish_normal.png";
	// shield
	this.removeShield();
};

/*------------------------ shield ------------------------*/

FlapLearn.PlayerMain.prototype.addShield = function () {
	FlapLearn.plrShield = true;
	this.alpha = 0.6;
	this.shieldActual = this.shieldInitTime;
};

FlapLearn.PlayerMain.prototype.removeShield = function () {
	FlapLearn.plrShield = false;
	this.alpha = 1.0;
};

/*------------------------ update ------------------------*/

FlapLearn.PlayerMain.prototype.flap = function () {
	this.vY = -FlapLearn.flapStr;
	// state
	this.flapDn = true;
	this.frameName = "playerFish_flap.png";
	if(Math.random()>0.5){
		FlapLearn.sndPlr.playGameSnd("flap_1");
	} else {
		FlapLearn.sndPlr.playGameSnd("flap_2");
	}	
};

FlapLearn.PlayerMain.prototype.unFlap = function () {
	// state
	if(this.flapDn){
		this.flapDn = false;
		this.frameName = "playerFish_normal.png";
	}	
};

/*------------------------ anims ------------------------*/

// obstacle hit

FlapLearn.PlayerMain.prototype.playHitAnim = function () {
	this.flapDn = false;
	this.frameName = "playerFish_hit.png";

	this.animTwn1 = this.game.add.tween(this);    
    this.animTwn1.to({angle:"+320", x:this.initX+400}, 2500, Phaser.Easing.Sinusoidal.Out);
    this.animTwn1.start();

	this.animTwn2 = this.game.add.tween(this);    
    this.animTwn2.to({y:FlapLearn.GROUND_Y-10}, 2500, Phaser.Easing.Bounce.Out);
    this.animTwn2.onComplete.add(this.hitAnimEnd, this);
    this.animTwn2.start();
};

FlapLearn.PlayerMain.prototype.hitAnimEnd = function () {
	setTimeout(this.hitAnimTimeEnd,1000);	
};

FlapLearn.PlayerMain.prototype.hitAnimTimeEnd = function () {
	FlapLearn.gameSignal.dispatch("ObstacleHitAnimEnd");
};

// correct answer hit

FlapLearn.PlayerMain.prototype.playAnswerCorrectHitAnim = function () {
	this.animTwn1 = this.game.add.tween(this);    
    this.animTwn1.to({angle:"+360"}, 600, Phaser.Easing.Sinusoidal.InOut);
    this.animTwn1.onComplete.add(this.playAnswerCorrectHitAnim2, this);
    this.animTwn1.start();
};

FlapLearn.PlayerMain.prototype.playAnswerCorrectHitAnim2 = function () {
	this.animTwn1 = this.game.add.tween(this);    
    this.animTwn1.to({x:FlapLearn.GW + 200, angle:-15}, 1500, Phaser.Easing.Sinusoidal.In);
    this.animTwn1.onComplete.add(this.AnswerCorrectHitAnimEnd, this);
    this.animTwn1.start();
};

FlapLearn.PlayerMain.prototype.AnswerCorrectHitAnimEnd = function () {
	setTimeout(this.AnswerCorrectHitAnimTimeEnd,1000);	
};

FlapLearn.PlayerMain.prototype.AnswerCorrectHitAnimTimeEnd = function () {
	FlapLearn.gameSignal.dispatch("AnswerHitAnimEnd");	
};

/*------------------------ update ------------------------*/

FlapLearn.PlayerMain.prototype.playerUpdate = function () {
	this.upRet = true;
	this.vY += FlapLearn.gravityStr * FlapLearn.dTm/2;
	this.y += this.vY * FlapLearn.dTm;
	this.vY += FlapLearn.gravityStr * FlapLearn.dTm/2;

	if(this.y > FlapLearn.GROUND_Y - FlapLearn.PLR_HIT) {
		// bounce
		if(FlapLearn.groundBounce){
			this.y = FlapLearn.GROUND_Y - FlapLearn.PLR_HIT;
			this.vY *= -FlapLearn.groundBounceVal;
		} else {
			this.upRet = false;
			FlapLearn.gameSignal.dispatch("ObstacleHit");
			FlapLearn.sndPlr.playGameSnd("hit");
		}
	} else if(this.y < 0){
		// top limit
		this.y = 0;
	}
	// update global var for hit tests
	FlapLearn.plrX = this.x;
	FlapLearn.plrY = this.y;

	// angle
	if(this.vY < 0){
		this.gAng = 20 * (this.vY/FlapLearn.flapStr);
	} else {
		this.gAng = 10 * (this.vY/FlapLearn.flapStr);
	}
	this.vA = (this.gAng - this.angle) * 0.1;
	this.angle += this.vA;

	// shield
	if(FlapLearn.plrShield){
		this.shieldActual -= this.game.time.elapsed;
		if(this.shieldActual <= 0){
			this.removeShield();
		}
	}

	return this.upRet;
};