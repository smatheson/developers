/*

TODO

- DONE

*/

FlapLearn.EnemyObstacle = function (game, enmType, positionID) {
    Phaser.Image.call(this, game, 0, 0, "mainSt", "tempEnemy_" + enmType.toString() + ".png");
	
	// vars
	this.isActive = false;
    this.upState = 0;
    this.eType = enmType;		// 1|2|3
    this.posID = positionID;
    this.hideW = 150;           // for show/hide check
    this.wX = 0;
    this.yAng = Math.random() * Math.PI;
    this.xRand =  -(FlapLearn.obstacleDensity * 0.1) + Math.round(Math.random() * (FlapLearn.obstacleDensity * 0.2));
    this.hitR = 57;
    // props
    this.yUp();
    this.updateWorldX();

    switch(enmType){
        case 1:
            this.anchor.setTo(86/168, 87/159);
            break;
        case 2:
            this.anchor.setTo(77/185, 95/157);
            break;
        case 3:
            this.anchor.setTo(69/138, 64/132);
            break;
    }
};

FlapLearn.EnemyObstacle.prototype = Object.create(Phaser.Image.prototype);
FlapLearn.EnemyObstacle.prototype.constructor = FlapLearn.EnemyObstacle;

/*------------------------ update ------------------------*/

FlapLearn.EnemyObstacle.prototype.updateWorldX = function () {
    this.wX = (FlapLearn.GW + this.posID * FlapLearn.obstacleDensity) + this.xRand;
    this.x = this.wX - FlapLearn.worldPos;
};

FlapLearn.EnemyObstacle.prototype.oUpdate = function () {
	this.upState = 0;
	this.x = this.wX - FlapLearn.worldPos;
	// y
    this.yUp();
    // pos check
    if(this.x < -this.hideW){
        // loop
        this.posID += FlapLearn.totalAnswers * 2;
        this.updateWorldX();
        // remove from stage
        this.upState = 2;
        this.isActive = false;
    } else if(!this.isActive && this.x < FlapLearn.GW + this.hideW){
        // add to stage
        this.upState = 1;
        this.isActive = true;
    }

    // try hit
    if(this.isActive && !FlapLearn.plrShield){
        if(FlapLearn.mathHelper.testHitCircPlr(this.x, this.y, this.hitR)) {
            this.upState = 3;
        }
    }

    return this.upState;    
};

FlapLearn.EnemyObstacle.prototype.yUp = function () {
    this.y = 410 + Math.cos(this.yAng) * 290;
	this.yAng += FlapLearn.trapsMove;
};

/*------------------------ destroy ------------------------*/

FlapLearn.EnemyObstacle.prototype.destroyObstacle = function () {
	this.kill();
};