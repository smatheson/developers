/*

TODO

- DONE

*/

FlapLearn.LifeIco = function (game,iX,iY) {
    Phaser.Image.call(this, game, iX, iY, "mainSt", "guiLifeIco.png");

    this.scaleTwn;

    this.anchor.setTo(0.5, 0.5);
};

FlapLearn.LifeIco.prototype = Object.create(Phaser.Image.prototype);
FlapLearn.LifeIco.prototype.constructor = FlapLearn.LifeIco;

/*------------------------ update ------------------------*/

FlapLearn.LifeIco.prototype.setActive = function (isActive) {
    if(isActive){
    	this.alpha = 1.0;
    } else {
    	if(this.alpha > 0.9){
    		this.alpha = 0.2;
	    	this.scale.setTo(0.2, 0.2);
	    	// anim
	    	this.scaleTwn = this.game.add.tween(this.scale);    
		    this.scaleTwn.to({x:1.0, y:1.0}, 400, Phaser.Easing.Back.Out);
		    this.scaleTwn.start();
    	}
    }
};