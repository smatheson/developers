FlapLearn.SoundPlayer = function (game) {
	this.soundOn = true;
	this.soundEnabled = true;
	this.sndsPool = [];
	this.sndIDsPool = [];

	if (game.device.webAudio) {
		this.soundEnabled = true;

		// init sounds
		for (var s = 0; s < FlapLearn.gameSnds.length; s++) {
			this.sndIDsPool.push(FlapLearn.gameSnds[s][0]);
			this.sndsPool.push(game.add.audio(FlapLearn.gameSnds[s][0]));
		}
	} else {
		this.soundEnabled = false;
	}
};

FlapLearn.SoundPlayer.prototype = {

	playGameSnd: function (sndID) {
		if (this.soundOn && this.soundEnabled) {
			if (this.sndIDsPool.indexOf(sndID) != -1) {
				this.sndsPool[this.sndIDsPool.indexOf(sndID)].play("", 0, 1.0, false);
			} else {
				console.log("Undefined Snd - " + sndID);
			}
		}
	}
};