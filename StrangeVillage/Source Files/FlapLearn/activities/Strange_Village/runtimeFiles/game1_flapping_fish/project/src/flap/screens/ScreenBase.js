FlapLearn.ScreenBase = function (game) {
    Phaser.Group.call(this, game);
    // vars
    this.isActive = true;	// for buttons disable during anims
    this.scrnTwn;
    // parts
    this.scrnOverlay = new Phaser.Image(this.game, 0, 0, "scrnOverlay");
    this.add(this.scrnOverlay);
};

FlapLearn.ScreenBase.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.ScreenBase.prototype.constructor = FlapLearn.ScreenBase;

/*------------------------ hide ------------------------*/

FlapLearn.ScreenBase.prototype.hideScrn = function () {
	FlapLearn.sndPlr.playGameSnd("menu_show");
    this.isActive = false;
	// tween
	this.scrnTwn = this.game.add.tween(this);    
    this.scrnTwn.to({y:-FlapLearn.GH}, 400, Phaser.Easing.Sinusoidal.In);
    this.scrnTwn.onComplete.add(this.hideScrnEnd, this);
    this.scrnTwn.start();
};

FlapLearn.ScreenBase.prototype.hideScrnEnd = function () {
	this.removeAll();
	this.destroy();
};

/*------------------------ show ------------------------*/

FlapLearn.ScreenBase.prototype.showScrn = function (showType) {
	FlapLearn.sndPlr.playGameSnd("menu_show");
    this.isActive = false;
	this.y = -FlapLearn.GH;
	// tween
	this.scrnTwn = this.game.add.tween(this);    
    this.scrnTwn.to({y:0}, 500, Phaser.Easing.Sinusoidal.Out);
    this.scrnTwn.onComplete.add(this.showScrnEnd, this);
    this.scrnTwn.start();
};

FlapLearn.ScreenBase.prototype.showScrnEnd = function () {
	this.isActive = true;
};