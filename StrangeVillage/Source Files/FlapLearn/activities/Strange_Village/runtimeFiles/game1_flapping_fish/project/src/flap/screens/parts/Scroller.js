

FlapLearn.Scroller = function (game) {
    Phaser.Group.call(this, game);

    // parts
    this.scrollBg = new Phaser.Image(this.game, 0, 0, "mainSt", "scrollBg.png");    
    this.add(this.scrollBg);

    this.scrollThumb = new Phaser.Image(this.game, 0, 0, "mainSt", "scrollThumb.png"); 
    this.scrollThumb.inputEnabled = true;
    this.scrollThumb.input.enableDrag(false,false,false,255,null,this.scrollBg);   
    this.add(this.scrollThumb);

    // position update
    this.x = Math.round((FlapLearn.GW-this.scrollBg.width)/2);
};

FlapLearn.Scroller.prototype = Object.create(Phaser.Group.prototype);
FlapLearn.Scroller.prototype.constructor = FlapLearn.Scroller;

FlapLearn.Scroller.prototype.getPerc = function () {
	return this.scrollThumb.x / (this.scrollBg.width-this.scrollThumb.width);
};