
function testInstallation() {
    var game = new Phaser.Game(1100, 825, Phaser.AUTO, 'canvas1');

	game.state.add('Boot', FlapLearn.Boot);
	game.state.add('Preloader', FlapLearn.Preloader);
	game.state.add('Game', FlapLearn.Game);

	game.state.start('Boot');
};

function parseDifficulty() {
    FlapLearn.mainFont = FlapLearn.mainSettingsData.GameFonts[0];
    FlapLearn.livesStart = FlapLearn.mainSettingsData.GameSettings.startLives[FlapLearn.diffLvl];
    FlapLearn.gravityStr = FlapLearn.mainSettingsData.GameSettings.gravity[FlapLearn.diffLvl];
    FlapLearn.flapStr = FlapLearn.mainSettingsData.GameSettings.flapStrength[FlapLearn.diffLvl];
    FlapLearn.horizontalSpd = FlapLearn.mainSettingsData.GameSettings.horizontalSpeed[FlapLearn.diffLvl];
    FlapLearn.trapsMove = FlapLearn.mainSettingsData.GameSettings.trapsVerticalSpeed[FlapLearn.diffLvl];
    FlapLearn.obstacleDensity = FlapLearn.mainSettingsData.GameSettings.obstaclesDensity[FlapLearn.diffLvl];
    FlapLearn.groundBounce = FlapLearn.mainSettingsData.GameSettings.groundBounce[FlapLearn.diffLvl];
    FlapLearn.groundBounceVal = FlapLearn.mainSettingsData.GameSettings.groundBouceValue[FlapLearn.diffLvl];
    console.log("Difficulty set:",FlapLearn.diffLvl);
}