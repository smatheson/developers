﻿

var runtimeFiles = "runtimeFiles";
var fileName = "runtimeUnity2.unity3d";
var runtimeURL;
var gameW = $(window).width();
var gameH = $(window).height();

$(window).load(function () {
	// perform required sizing functions on initial load
   
});



// dynamically load runtime files and start canvas
function startUnityTemplate() {

 // check for multichapter data
    if (map_getNumberOfChapters() < 0) {
        var runtimeDir = map_getRuntimeDir();
        runtimeURL = runtimeFiles + "/" + runtimeDir + "/" + fileName;
       
        if (typeof unityObject != "undefined") {
            unityObject.embedUnity("unityPlayer", runtimeURL, gameW, gameH);
        } else {
        getUnity();
            unityObject.embedUnity("unityPlayer", runtimeURL, gameW, gameH);
        }


    } else {
        alert("Multi-chapter data is not supported for this template");
    }

}



function getUnity() {
    if (typeof unityObject != "undefined") {
        return unityObject.getObjectById("unityPlayer");
    }
    return null;
}


function resizeWrapper() {
	

}

$(window.parent).resize(function () {
	
	//trigger any local resize events
    onGameWindowResize();

});

